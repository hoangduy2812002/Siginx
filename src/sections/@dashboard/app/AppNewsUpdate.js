// @mui
import PropTypes from "prop-types";
import { Box, Stack, Card, Typography, CardHeader } from "@mui/material";
import { Link } from "react-router-dom";

// utils
// components

// ----------------------------------------------------------------------

AppNewsUpdate.propTypes = {
  title: PropTypes.string,
  subheader: PropTypes.string,
  list: PropTypes.array.isRequired,
};

export default function AppNewsUpdate({ title, subheader, list, ...other }) {
  return (
    <Card {...other} sx={{ borderRadius: 5, boxShadow: 10 }}>
      <CardHeader title={title} subheader={subheader} />

      {/* <Scrollbar> */}
      <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
        {list?.map((news) => (
          <NewsItem key={news._id} news={news} />
        ))}
      </Stack>
      {/* </Scrollbar> */}
    </Card>
  );
}

// ----------------------------------------------------------------------

NewsItem.propTypes = {
  news: PropTypes.shape({
    description: PropTypes.string,
    image: PropTypes.string,
    postedAt: PropTypes.instanceOf(Date),
    title: PropTypes.string,
  }),
};

function NewsItem({ news }) {
  const { _id, processingTime, projectName, contractStatus } = news;

  return (
    <Link to={`/workDetailsPage/${_id}`} style={{ textDecoration: "none" }}>
      <Stack
        direction="row"
        alignItems="center"
        spacing={2}
        sx={{
          boxShadow: 2,
          borderRadius: 2,
          padding: 0.6,
          mt: -3,
          mb: 3.5,
          mr: 3,
        }}
      >
        <Box sx={{ flexGrow: 1, }}>
          <Typography variant="body2" sx={{ color: "black",fontSize:18 }} noWrap>
            <span style={{ color: "blue",fontFamily:'serif' }}>Dự án: </span>
            {projectName}
          </Typography>

          <Typography variant="body2" sx={{ color: "black",fontSize:18 }} noWrap>
            <span style={{ color: "green" ,fontFamily:'serif',}}>Hiện trạng: </span>
            {contractStatus.statusName}
          </Typography>
          <Typography
            variant="caption"
            sx={{ pr: 3, flexShrink: 0, color: "black",fontSize:18 }}
          >
            <span style={{ color: "red" ,fontFamily:'serif',}}>Thời gian xử lý: </span>
            {processingTime}
          </Typography>
        </Box>
      </Stack>
    </Link>
  );
}
