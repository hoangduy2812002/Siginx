import PropTypes from "prop-types";
// @mui
import { styled } from "@mui/material/styles";
import {
  Toolbar,
  Tooltip,
  Typography,
  Card,
  CardHeader,
  Divider,
  CardContent,
} from "@mui/material";
// component
import Iconify from "../../../ui-component/iconify";
import Axios from "utils/Axios";
import { toast } from "react-toastify";
import React, { useState } from "react";
import { Box } from "@mui/system";
import SearchInput from "layout/MainLayout/Header/components/SearchInput";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import UserSearchBox from "layout/MainLayout/Header/UserSearchBox";

// ----------------------------------------------------------------------
const scrollbar = {
  "::-webkit-scrollbar": {
    width: "8px",
  },
  ":hover::-webkit-scrollbar-thumb": {
    " -webkit-border-radius": "5px",
    borderRadius: "5px",
    background: "#dee2e3",
  },
  "::-webkit-scrollbar-thumb:window-inactive": {
    background: "#dee2e3",
  },
};

const StyledRoot = styled(Toolbar)(({ theme }) => ({
  height: 96,
  display: "flex",
  justifyContent: "space-between",
  padding: theme.spacing(0, 1, 0, 3),
}));

// ----------------------------------------------------------------------

WorkListToolbar.propTypes = {
  numSelected: PropTypes.number,
  filterName: PropTypes.string,
  onFilterName: PropTypes.func,
};

export default function WorkListToolbar({
  numSelected,
  selected,
  onChange,
  page,
  limit,
}) {
  const [searchList, setSearchList] = useState([]);
  const [open, setOpen] = useState(false);
  const [isFocusPopup, setFocusPopup] = useState(false);

  const [openDialog, setOpenDialog] = React.useState(false);

  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleCloseDialogDelete = () => {
    deleteUser();
    setOpenDialog(false);
  };


  const searchAllUser = async () => {
    const response = await Axios.Profile.getAllUser(0, 20, -1, "", "");
    if (response.status === 200) {
      setSearchList(response.data.listItem);
    }
  };

  const handleSearch = async (e) => {
    const response = await Axios.Profile.getAllUser(0, 20, -1, e.target.value, "");
    setSearchList(response.data.listItem);
  };

  const handleOpen = (event) => {
    searchAllUser();
    setOpen(event.currentTarget);
  };

  const handleClose = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
    }
  };
  const handleClose2 = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
      setFocusPopup(false);
    } else {
      setFocusPopup(false);
    }
  };

  const deleteUser = async () => {
    const response = await Axios.Profile.deleteUser({ listId: selected });

    if (response.status === 200) {
      onChange(page, limit, -1, "");
      toast.success("Xoá người dùng thành công!");
    } else {
      toast.error("Xoá người dùng thất bại!");
    }
  };
  return (
    <StyledRoot
      sx={{
        ...(numSelected > 0 && {
          borderRadius: 50,
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography component="div" variant="subtitl1">
          <span style={{ color: "red", fontWeight: "bold" }}>
            {numSelected}
          </span>{" "}
          người dùng đã chọn
        </Typography>
      ) : (
        <Box onFocus={handleOpen} onBlur={handleClose}>
          <SearchInput position="start" onInput={handleSearch} />
          <Card
            hidden={!open}
            sx={{
              width: 370,
              position: "fixed",
              mt: 1,
              p: 0,
              borderRadius: 2,
              boxShadow: "0 0 8px #976235",
              maxHeight: "40vh",
              zIndex: 99999,
            }}
          >
            <Box
              onBlur={handleClose}
              onMouseEnter={() => setFocusPopup(true)}
              onMouseLeave={handleClose2}
            >
              <CardHeader
                title={
                  <Typography fontWeight={500} fontSize={16}>
                    {"Danh Sách Tìm Kiếm"}
                  </Typography>
                }
                sx={{ pb: 1, height: 24}}
              />
              <Divider />
              <CardContent
                sx={{
                  p: "0 !important",
                  maxHeight: "calc(40vh - (14*2px))",
                  overflow: "auto",
                  ...scrollbar,
                }}
              >
                {searchList?.map((searchData, index) => (
                  <UserSearchBox key={index} searchData={searchData} />
                ))}
              </CardContent>
            </Box>
          </Card>
        </Box>
      )}

      {numSelected > 0 ? (
        <div>
          <Tooltip title="Xoá">
            <Button
              // variant="outlined" 
              sx={{ color: "red", border: "none" }}
              onClick={handleClickOpen}
            >
              <Iconify icon="eva:trash-2-fill" />
            </Button>
          </Tooltip>
          <Dialog
            open={openDialog}
            onClose={handleCloseDialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle width={400} id="alert-dialog-title">
              <Typography
                component="div"
                sx={{ fontSize: 20 }}
                variant="subtitl1"
              >
                {"Bạn có xác nhận xoá?"}
              </Typography>
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <Typography
                  component="span"
                  sx={{ fontSize: 20, fontFamily: "bold" }}
                >
                  <span style={{ color: "red", fontWeight: "bold" }}>
                    {numSelected}
                  </span>{" "}
                  người dùng đã chọn
                </Typography>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseDialogDelete}>Xác nhận</Button>
              <Button
                onClick={handleCloseDialog}
                sx={{ color: "red" }}
                autoFocus
              >
                Huỷ
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      ) : (
        ""
      )}
    </StyledRoot>
  );
}
