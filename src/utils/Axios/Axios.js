import Config from "./config";
import axios from "axios";

let localToken = `${localStorage.getItem("account")}`;
const dataToken = JSON.parse(localToken) ? JSON.parse(localToken) : "";

class Axios {
  static async post(url, data, headers) {
    const response = await axios
      .post(`${Config.URL}${url}`, data, {
        headers: {
          ...headers,
          ...Config.HEADERS,
        },
      })
      .then((result) => result)
      .catch((error) => error.response);
    if (response.status === 401 && response.data.code === -1) {
      const responseRefreshToken = await this.postNoToken(
        "/api/v1/auth/refreshToken",
        { refreshToken: dataToken.refreshToken }
      );
      if(responseRefreshToken.status === 401 && responseRefreshToken.data.code === -1){
        localStorage.removeItem("account");
        window.location = "/login";
      }

      let account = {};
      account = dataToken;
      account.accessToken = responseRefreshToken.data.data.accessToken;

      localStorage.setItem("account", JSON.stringify(account));

      const CONFIG = {
        HEADERS: {
          Accept: "application/json",
          "Content-Type": "application/json; charset=utf-8",
          Authorization: "Bearer " + account.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
        },
      };

      let responseAccessToken = await axios
        .post(`${Config.URL}${url}`, data, {
          headers: {
            ...headers,
            ...CONFIG.HEADERS,
          },
        })
        .then((result) => result)
        .catch((error) => error.response);
      return responseAccessToken.data;
    }
    return response;
  }

  static async postNoToken(url, data, headers) {
    const response = await axios
      .post(`${Config.URL}${url}`, data, {
        headers: {
          ...headers,
        },
      })
      .then((result) => result)
      .catch((error) => error.response);
    return response;
  }

  static async get(url, headers) {
    const response = await axios
      .get(`${Config.URL}${url}`, {
        data: null,
        headers: {
          ...headers,
          ...Config.HEADERS,
        },
      })
      .then((result) => result)
      .catch((error) => error.response);
    if (response.status === 401 && response.data.code === -1) {
      const responseRefreshToken = await this.postNoToken(
        "/api/v1/auth/refreshToken",
        { refreshToken: dataToken.refreshToken }
      );
      if(responseRefreshToken.status === 401 && responseRefreshToken.data.code === -1){
        localStorage.removeItem("account");
        window.location = "/login";
      }

      let account = {};
      account = dataToken;
      account.accessToken = responseRefreshToken.data.data.accessToken;
      localStorage.setItem("account", JSON.stringify(account));
      const CONFIG = {
        HEADERS: {
          Accept: "application/json",
          "Content-Type": "application/json; charset=utf-8",
          Authorization: "Bearer " + account.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
        },
      };

      let responseAccessToken = await axios
        .get(`${Config.URL}${url}`, {
          data: null,
          headers: {
            ...headers,
            ...CONFIG.HEADERS,
          },
        })
        .then((result) => result)
        .catch((error) => error.response);
      return responseAccessToken.data;
    }
    return response.data;
  }

  static async getNewToken(url,token, headers) {
    const ConfigNewToken = {
      HEADERS: {
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
        Authorization: "Bearer " + token,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
      },
    };
    const response = await axios
      .get(`${Config.URL}${url}`, {
        data: null,
        headers: {
          ...headers,
          ...ConfigNewToken.HEADERS,
        },
      })
      .then((result) => result)
      .catch((error) => error.response);
    if (response.status === 401 && response.data.code === -1) {
      const responseRefreshToken = await this.postNoToken(
        "/api/v1/auth/refreshToken",
        { refreshToken: dataToken.refreshToken }
      );
      if(responseRefreshToken.status === 401 && responseRefreshToken.data.code === -1){
        localStorage.removeItem("account");
        window.location = "/login";
      }

      let account = {};
      account = dataToken;
      account.accessToken = responseRefreshToken.data.data.accessToken;
      localStorage.setItem("account", JSON.stringify(account));
      const CONFIG = {
        HEADERS: {
          Accept: "application/json",
          "Content-Type": "application/json; charset=utf-8",
          Authorization: "Bearer " + account.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
        },
      };

      let responseAccessToken = await axios
        .get(`${Config.URL}${url}`, {
          data: null,
          headers: {
            ...headers,
            ...CONFIG.HEADERS,
          },
        })
        .then((result) => result)
        .catch((error) => error.response);
      return responseAccessToken.data;
    }
    return response.data;
  }
}

export default Axios;
