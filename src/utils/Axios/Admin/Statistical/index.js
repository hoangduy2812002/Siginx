import Axios from "utils/Axios/Axios";

class Statistical {
    
    static getAllStatistical = async () => Axios.get(`/api/v1/statistical`);

  }
  
export default Statistical;