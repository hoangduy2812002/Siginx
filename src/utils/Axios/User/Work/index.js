import Axios from "utils/Axios/Axios";

class Work {
    static getAllWork = async (page,limit,sortOrder,search,sortProperty) => 
    Axios.get("/api/v1/project?page="+page+"&limit="+limit+"&sortOrder="+sortOrder+"&search="+search+"&sortProperty="+sortProperty);

    static createWork = async (data) => Axios.post("/api/v1/project",data);

    static deleteWork = async (data) => Axios.post("/api/v1/project/delete",data);

    static updateWork = async (id,data) => Axios.post("/api/v1/project/"+id,data);

    static getOneWork = async (id) => Axios.get(`/api/v1/project/${id}`);

    static getAllContactStatus = async (id) => Axios.get(`/api/v1/project/getAllContractStatus`);
  }
  
export default Work;