import Axios from "utils/Axios/Axios";

class Profile {

    static getAllUser = async (page,limit,sortOrder,search,sortProperty) => 
    Axios.get("/api/v1/user?page="+page+"&limit="+limit+"&sortOrder="+sortOrder+"&search="+search+"&sortProperty="+sortProperty);

    static changePassword = async (data) => Axios.post("/api/v1/auth/changePassword",data);

    static registerUser = async (data) => Axios.post("/api/v1/user",data);

    static getOneUser = async (id) => Axios.get(`/api/v1/user/${id}`);

    static deleteUser = async (data) => Axios.post("/api/v1/user/delete",data);

    static updateUser = async (id,data) => Axios.post("/api/v1/user/"+id,data);

    static resetPassword = async (data) => Axios.post("/api/v1/user/resetPassword",data);

  }
  
export default Profile;