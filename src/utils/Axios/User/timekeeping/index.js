import Axios from "utils/Axios/Axios";

class Timekeeping {
  static getAllTimekeeping = async (month) => Axios.get(`/api/v1/timekeeping?month=${month}`);

  static attenDance = async (data) => Axios.post("/api/v1/timekeeping",data);

  static deleteTimeKeeping = async (id) => Axios.post(`/api/v1/timekeeping/delete/${id}`);
}

export default Timekeeping;
