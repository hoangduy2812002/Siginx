import Profile from "./User/Profile";
import Work from "./User/Work";
import Timekeeping from "./User/timekeeping";
import Statistical from "./Admin/Statistical";
const Axios = {
    Profile,
    Work,
    Timekeeping,
    Statistical
};
  
export default Axios;