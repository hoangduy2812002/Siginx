let localToken = `${localStorage.getItem("account")}`;
const dataToken = JSON.parse(localToken) ? JSON.parse(localToken) : "";

const CONFIG = {
  // URL: "http://localhost:3001",
  URL: "http://103.176.179.174:3001",
  HEADERS: {
    Accept: "application/json",
    "Content-Type": "application/json; charset=utf-8",
    Authorization: "Bearer " + dataToken.accessToken,
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
  },
};

export default CONFIG;
