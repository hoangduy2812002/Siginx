import { useState } from "react";

export default function useLogin() {
  const getAccount = () => {
    let accountString = localStorage.getItem("account");
    const account = JSON.parse(accountString);
    return account;
  };

  const [account, setAccount] = useState(getAccount());

  const saveAccount = async (account, email, password, remember) => {
    if (!account) {
      return;
    }
    if (remember === true) {
      account.remember = true;
      account.email = email;
      account.password = password;
    } else {
      account.remember = false;
      account.email = '';
      account.password = '';
    }
    account.status = true;
    localStorage.setItem("account", JSON.stringify(account));
    setAccount(account);
    return;
  };

  const logout = () => {
    // if (account.remember === false) {
      // localStorage.removeItem("account");
      account.status = false;
      // setAccount(account);
      // window.location = "/login";
    // } else {
    //   account.status = false;
    
    // }
    localStorage.setItem("account", JSON.stringify(account));
    setAccount(account);
    window.location = "/login";
  };

  return {
    account,
    setAccount: saveAccount,
    logout,
  };
}
