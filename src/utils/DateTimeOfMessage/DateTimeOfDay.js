import styled from "styled-components";

const DateTime = styled("span")(({ theme }) => ({
    fontWeight: 700,
    fontSize: 12,
    color: "#555555",
}));

export default function DateTimeOfDay({ dateTime }) {

    let today = new Date(dateTime);
    let date =
      today.getDate() +
      " / " +
      parseInt(today.getMonth() + 1) +
      " / " +
      today.getFullYear()

  return <DateTime >{date}</DateTime>;
}
