import styled from "styled-components";

const DateTime = styled("span")(({ theme }) => ({
    fontWeight: 700,
    fontSize: 12,
    color: "#555555",
}));

export default function DateTimeOfMessage({ dateTime }) {

    let today = new Date(dateTime);
    let date =
      today.getDate() +
      " / " +
      parseInt(today.getMonth() + 1) +
      " / " +
      today.getFullYear() +
      "    -  ( " +
      today.getHours() +
      " giờ : " +
      today.getMinutes() +
      " phút : " +
      today.getSeconds() +
      " giây )";

  return <DateTime >{date}</DateTime>;
}
