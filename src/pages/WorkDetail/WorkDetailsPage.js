// import { format } from "date-fns";
import {
  Backdrop,
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import SearchInput from "layout/MainLayout/Header/components/SearchInput";
import WorkSearchBox from "layout/MainLayout/Header/WorkSearchBox";
import React, { useState } from "react";
import { useEffect } from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router-dom";
import Axios from "utils/Axios";
import DateTimeOfMessage from "utils/DateTimeOfMessage/DateTimeOfMessage";
import { DialogEditWork } from "views/workflowManagement/components/DialogEditWork";
import BasicSpeedDial from "./BasicSpeedDial";

const scrollbar = {
  "::-webkit-scrollbar": {
    width: "8px",
  },
  ":hover::-webkit-scrollbar-thumb": {
    " -webkit-border-radius": "5px",
    borderRadius: "5px",
    background: "#dee2e3",
  },
  "::-webkit-scrollbar-thumb:window-inactive": {
    background: "#dee2e3",
  },
};
const LayoutFormTwoField = ({ children }) => {
  return (
    <Grid container spacing={2} sx={{ width: "100%", mb: 2 }}>
      {children}
    </Grid>
  );
};

export default function WorkDetailsPage() {
  const { _id } = useParams();

  const [project, setProject] = useState({});

  const [isEdit, setIsEdit] = useState(false);

  const [open, setOpen] = useState(false);
  const [isFocusPopup, setFocusPopup] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [showLoading, setShowLoading] = useState(false);

  useEffect(() => {
    getOneProject(_id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_id]);

  const getOneProject = async (_id) => {
    if (_id !== undefined) {
      setShowLoading(() => true);
      const response = await Axios.Work.getOneWork(_id);
      if (response.status === 200) {
        setShowLoading(() => false);
        setProject(response.data);
      } else {
        setShowLoading(() => false);
        setProject(null);
      }
    }
  };

  const handleOpen = (event) => {
    searchAllWork();
    setOpen(event.currentTarget);
  };

  const searchAllWork = async () => {
    const response = await Axios.Work.getAllWork(0, 30, -1, "", "");
    if (response.status === 200) {
      setSearchList(response.data.listItem);
    } else {
    }
  };

  const handleSearch = async (e) => {
    const response = await Axios.Work.getAllWork(
      0,
      100,
      -1,
      e.target.value,
      ""
    );
    if (response.status === 200) {
      setSearchList(response.data.listItem);
    } else {
    }
  };

  const handleClose = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
    }
  };
  const handleClose2 = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
      setFocusPopup(false);
    } else {
      setFocusPopup(false);
    }
  };

  const handleUpdateWork = () => {
    setIsEdit(true);
  };

  return (
    <>
      <Helmet>
        <title> Trang chủ | Chi tiết công việc </title>
      </Helmet>
      <Box onFocus={handleOpen} onBlur={handleClose}>
        <SearchInput onInput={handleSearch} />
        <Card
          hidden={!open}
          sx={{
            width: 400,
            position: "fixed",
            mt: 1,
            p: 0,
            borderRadius: 2,
            boxShadow: "0 0 8px #976235",
            maxHeight: "40vh",
          }}
        >
          <Box
            onBlur={handleClose}
            onMouseEnter={() => setFocusPopup(true)}
            onMouseLeave={handleClose2}
          >
            <CardHeader
              title={
                <Typography fontWeight={500} fontSize={16}>
                  {"Danh Sách Tìm Kiếm"}
                </Typography>
              }
              sx={{ pb: 1, height: 24 }}
            />
            <Divider />
            <CardContent
              sx={{
                p: "0 !important",
                maxHeight: "calc(40vh - (14*2px))",
                overflow: "auto",
                ...scrollbar,
              }}
            >
              {searchList?.map((searchData, index) => (
                <WorkSearchBox key={index} searchData={searchData} />
              ))}
            </CardContent>
          </Box>
        </Card>
      </Box>
      
      <Box sx={{ width: "100%" }}>
        <Box
          sx={{
            background: "white",
            width: "100%",
            mt: 4,
            borderRadius: "12px",
            boxShadow: 5,
            padding: 2,
          }}
        >
          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Dự án:</Box>
              <Box sx={{ color: "black", ml: 2 }}>{project.projectName}</Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Chủ đầu tư:</Box>
              <Box sx={{ color: "black", ml: 2 }}>{project.investor}</Box>
            </Grid>
          </LayoutFormTwoField>
          <Divider sx={{ mb: 1.5 }} />

          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Đầu mối:</Box>
              <Box sx={{ color: "black", ml: 2 }}>{project.intermediary}</Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Hiện trạng:</Box>{" "}
              <Box sx={{ color: "black", ml: 2 }}>{project?.contractStatus?.statusName}</Box>
            </Grid>
          </LayoutFormTwoField>
          <Divider sx={{ mb: 1.5 }} />
          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Tiến độ kỹ thuật:
              </Box>
              <Box sx={{ color: "black", ml: 2 }}>
                {project.technicalProgress}
              </Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Tiến độ lắp đặt:
              </Box>{" "}
              <Box sx={{ color: "black", ml: 2 }}>
                {project.installationProgress}
              </Box>
            </Grid>
          </LayoutFormTwoField>
          <Divider sx={{ mb: 1.5 }} />
          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Tồn đọng kỹ thuật / thiết bị:
              </Box>{" "}
              <Box sx={{ color: "black", ml: 2 }}>{project.issue}</Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Hướng xử lý:</Box>
              <Box sx={{ color: "black", ml: 2 }}>{project.solution}</Box>
            </Grid>
          </LayoutFormTwoField>
          <Divider sx={{ mb: 1.5 }} />

          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Thời gian xử lý:
              </Box>{" "}
              <Box sx={{ color: "black", ml: 2 }}>{project.processingTime}</Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Giao hàng - Lắp Đặt - Bàn giao - Nghiệm Thu:
              </Box>
              <Box sx={{ color: "black", ml: 2 }}>{project.projectStatus}</Box>
            </Grid>
          </LayoutFormTwoField>
          <Divider sx={{ mb: 1.5 }} />

          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Ngày tạo: </Box>
              <Box sx={{ color: "black", ml: 2 }}>
                {" "}
                <DateTimeOfMessage dateTime={project.createdAt} />
              </Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>Người tạo: </Box>
              <Box sx={{ color: "black", ml: 2 }}>
                {project.createdBy?.fullname} {" - ("}{" "}
                {project.createdBy?.email} {" )"}
              </Box>
            </Grid>
          </LayoutFormTwoField>
          <Divider sx={{ mb: 1.5 }} />

          <LayoutFormTwoField>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Ngày cập nhật:{" "}
              </Box>
              <Box sx={{ color: "black", ml: 2 }}>
                <DateTimeOfMessage dateTime={project.updatedAt} />{" "}
              </Box>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={5.5}>
              <Box sx={{ color: "red", fontWeight: "bold" }}>
                Người cập nhật:{" "}
              </Box>
              <Box sx={{ color: "black", ml: 2 }}>
                {project.updatedBy?.fullname}{" "}
                {project.updatedBy !== null
                  ? " - ( " + project.updatedBy?.email + " )"
                  : "Chưa có người cập nhật"}
              </Box>
            </Grid>
          </LayoutFormTwoField>
        </Box>
      </Box>

      <BasicSpeedDial onchange={handleUpdateWork} />

      <DialogEditWork
        key={1}
        open={isEdit}
        setOpen={setIsEdit}
        project={project}
        getOneProject={getOneProject}
      />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
}
