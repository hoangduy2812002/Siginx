import { useState } from "react";
// @mui
import {
  Stack,
  IconButton,
  InputAdornment,
  TextField,
  Checkbox,
  FormControlLabel,
  Button,
  Backdrop,
  CircularProgress,
} from "@mui/material";
// components
import useLogin from "../../../utils/Login/useLogin";
import useValidator from "../../../utils/Validator";
// import { gapi } from "gapi-script";
import Iconify from "../../../ui-component/iconify/Iconify";
import Axios from "utils/Axios/Axios";
import { useEffect } from "react";

// ----------------------------------------------------------------------

export const LoginForm = () => {
  const { account, setAccount } = useLogin();

  const { validate } = useValidator();

  const [showPassword, setShowPassword] = useState(false);

  const [showLoading, setShowLoading] = useState(false);

  const [remember, setRemember] = useState(false);

  const [inputValues, setInputValues] = useState({
    email: "",
    password: "",
  });

  useEffect(() => {
    if (account) {
      setInputValues(account);
    } else {
      setInputValues({
        email: "",
        password: "",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [errors, setErrors] = useState({
    email: "",
    password: "",
  });

  useEffect(() => {
    if (account) {
      if (account.remember === false) {
        setRemember(false);
      } else {
        setRemember(true);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const loginWithAccount = async () => {
    if (inputValues.email === "") {
      inputValues.email = account.email;
    }

    if (inputValues.password === "") {
      inputValues.password = account.password;
    }
    const response = await Axios.postNoToken("/api/v1/auth/login", inputValues);

    if (response.status === 400 && response.data.code === 3) {
      setErrors({
        email: "Email không đúng!",
        password: "",
      });

      setShowLoading(() => false);
      return true;
    }
    if (response.status === 400 && response.data.code === 4) {
      setErrors({
        email: "",
        password: "Password không đúng!",
      });
      setShowLoading(() => false);
      return true;
    } else {
      const responseDetail = await Axios.getNewToken(
        "/api/v1/auth/userLoginDetail",
        response.data.data.accessToken
      );
      if (responseDetail.status === 200) {
        response.data.data._id = responseDetail.data._id;
        response.data.data.fullname = responseDetail.data.fullname;
        response.data.data.email = responseDetail.data.email;
        response.data.data.role = responseDetail.data.role;
        setAccount(response.data.data, inputValues.email, inputValues.password, remember);

      } else {

      }
      window.location = "/home";

      return true;
    }
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    let target = Array.from(e.target);
    let validForm = true;
    let error = {};
    target.forEach((data) => {
      if (data.name) {
        let err = validate(data.name, data.value);
        if (err) {
          error[data.name] = err;
          validForm = false;
        }
      }
    });
    setErrors(() => ({ ...errors, ...error }));
    if (validForm) {
      setShowLoading(() => true);
      loginWithAccount();
    } else {
    }
  };

  const handleOnInput = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
    setErrors({ ...errors, [name]: validate(name, value) });
  };

  return (
    <>
      <form onSubmit={handleOnSubmit}>
        <Stack spacing={3}>
          <TextField
            name="email"
            label="Email"
            value={inputValues.email}
            placeholder="Nhập email"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Iconify icon={"bxs:user"} />
                </InputAdornment>
              ),
            }}
            autoComplete="none"
            error={errors.email ? true : false}
            helperText={errors.email}
            onInput={handleOnInput}
            autoFocus
          />
          <TextField
            name="password"
            label="Mật khẩu"
            value={inputValues.password}
            type={showPassword ? "text" : "password"}
            placeholder="Nhập mật khẩu"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Iconify icon={"bxs:lock"} />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={() => setShowPassword(!showPassword)}
                    edge="end"
                  >
                    <Iconify
                      icon={showPassword ? "eva:eye-fill" : "eva:eye-off-fill"}
                    />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            autoComplete="none"
            error={errors.password ? true : false}
            helperText={errors.password}
            onInput={handleOnInput}
          />
        </Stack>

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          sx={{ my: 2 }}
        >
          {account && account.remember === true ? (
            <FormControlLabel
              control={
                <Checkbox
                  name="remember"
                  defaultChecked
                  value={account.remember}
                  onChange={() => setRemember(!account.remember)}
                />
              }
              label="Ghi nhớ tài khoản"
            />
          ) : (
            <FormControlLabel
              control={
                <Checkbox
                  name="remember"
                  value={remember}
                  onChange={() => setRemember(!remember)}
                />
              }
              label="Ghi nhớ tài khoản"
            />
          )}
          {/* <Link
            to={"/forgot-password"}
            variant="subtitle2"
            style={{ textDecoration: "none", color: "#1b74e4" }}
          >
            Quên mật khẩu?
          </Link> */}
        </Stack>

        <Button
          className="btn-orange"
          fullWidth
          size="large"
          type="submit"
          variant="contained"
          sx={{ marginBottom: 15 }}
        >
          Đăng Nhập
        </Button>
      </form>

      {/* <Snackbar open={showAlert} autoHideDuration={2000} onClose={handleClose}> */}
      {/* <Alert
          onClose={handleClose}
          severity={form.status}
          sx={{ width: "100%" }}
        >
          {form.message}
        </Alert> */}
      {/* </Snackbar> */}

      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};
