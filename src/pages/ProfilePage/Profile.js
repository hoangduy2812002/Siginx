import {
  Backdrop,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import Iconify from "../../ui-component/iconify/Iconify";

import useValidator from "../../utils/Validator";
import Axios from "utils/Axios";
import { Helmet } from "react-helmet";
import { useParams } from "react-router-dom";
import SearchInput from "layout/MainLayout/Header/components/SearchInput";
import UserSearchBox from "layout/MainLayout/Header/UserSearchBox";
import useLogin from "utils/Login/useLogin";

const styleInputFullField = {
  width: "55%",
  mb: 3,
  mr: 3,
};

const scrollbar = {
  "::-webkit-scrollbar": {
    width: "8px",
  },
  ":hover::-webkit-scrollbar-thumb": {
    " -webkit-border-radius": "5px",
    borderRadius: "5px",
    background: "#dee2e3",
  },
  "::-webkit-scrollbar-thumb:window-inactive": {
    background: "#dee2e3",
  },
};

export default function Profile() {
  const { _id } = useParams();

  const { account } = useLogin();

  const { validate } = useValidator();

  const [userEdit, setUserEdit] = useState({});
  const [searchList, setSearchList] = useState([]);
  const [open, setOpen] = useState(false);
  const [isFocusPopup, setFocusPopup] = useState(false);
  const [showLoading, setShowLoading] = useState(false);

  // const [userDetail, setUserDetail] = useState({});

  const [openDialog, setOpenDialog] = React.useState(false);

  useEffect(() => {
    getOneProject(_id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_id]);

  const getOneProject = async (_id) => {
    if (_id !== undefined) {
      setShowLoading(() => true);
      const response = await Axios.Profile.getOneUser(_id);

      if (response.status === 200) {
        setShowLoading(() => false);

        setInputValues(response.data);
        setUserEdit(response.data);
      } else {
        setShowLoading(() => false);
        // setUserDetail(null);
      }
    }
  };

  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const [inputValues, setInputValues] = useState({
    email: "",
    fullname: "",
  });

  const [errors, setErrors] = useState({
    newPassword: "",
    confirmPassword: "",
  });

  const handleOnInput = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
    setErrors({ ...errors, [name]: validate(name, value) });
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    let target = Array.from(e.target);
    let validForm = true;
    let error = {};
    target.forEach((data) => {
      if (data.name) {
        let err = validate(data.name, data.value);
        if (err) {
          error[data.name] = err;
          validForm = false;
        }
      }
    });
    setErrors(() => ({ ...errors, ...error }));

    if (validForm) {
      handleClickOpen();
    } else {
      toast.error("Vui lòng điền đầy đủ thông tin!");
    }
  };
  const updateUser = async () => {
    setShowLoading(() => true);
    const response = await Axios.Profile.updateUser(_id, userEdit);
    if (response.status === 200) {
      setShowLoading(() => false);
      getOneProject(_id);
      setOpenDialog(false);
      toast.success("Cập nhật người dùng thành công");
    } else {
      setShowLoading(() => false);
      setOpenDialog(false);
      setErrors({
        email: "Email đã tồn tại!",
      });
    }
  };

  const handleClose = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
    }
  };
  const handleClose2 = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
      setFocusPopup(false);
    } else {
      setFocusPopup(false);
    }
  };

  const searchAllUser = async () => {
    const response = await Axios.Profile.getAllUser(0, 20, -1, "", "");
    if (response.status === 200) {
      setSearchList(response.data.listItem);
    }
  };

  const handleSearch = async (e) => {
    const response = await Axios.Profile.getAllUser(
      0,
      20,
      -1,
      e.target.value,
      ""
    );
    setSearchList(response.data.listItem);
  };

  const handleOpen = (event) => {
    searchAllUser();
    setOpen(event.currentTarget);
  };

  return (
    <>
      <form onSubmit={handleOnSubmit}>
        <Helmet>
          <title> Thông tin cá nhân </title>
        </Helmet>
        {account.role === "admin" ? (
          <Box onFocus={handleOpen} onBlur={handleClose}>
            <SearchInput position="start" onInput={handleSearch} />
            <Card
              hidden={!open}
              sx={{
                width: 400,
                position: "fixed",
                mt: 1,
                p: 0,
                borderRadius: 2,
                boxShadow: "0 0 8px #976235",
                maxHeight: "40vh",
                zIndex: 99,
              }}
            >
              <Box
                onBlur={handleClose}
                onMouseEnter={() => setFocusPopup(true)}
                onMouseLeave={handleClose2}
              >
                <CardHeader
                  title={
                    <Typography fontWeight={500} fontSize={16}>
                      {"Danh Sách Tìm Kiếm"}
                    </Typography>
                  }
                  sx={{ pb: 1, height: 24 }}
                />
                <Divider />
                <CardContent
                  sx={{
                    p: "0 !important",
                    maxHeight: "calc(40vh - (14*2px))",
                    overflow: "auto",
                    ...scrollbar,
                  }}
                >
                  {searchList?.map((searchData, index) => (
                    <UserSearchBox key={index} searchData={searchData} />
                  ))}
                </CardContent>
              </Box>
            </Card>
          </Box>
        ) : (
          ""
        )}

        <Box
          margin="auto"
          position="relative"
          // width="50%"
          maxWidth={580}
          height="50%"
          boxShadow={3}
          borderRadius={3}
          padding={3}
          sx={{ mt: 3 }}
        >
          <Box>
            <Typography
              component="div"
              sx={{ fontWeight: "bold", fontSize: "20" }}
            >
              Thông tin cá nhân
            </Typography>
            <Box sx={{ textAlign: "center", mt: 3 }}>
              <TextField
                component="div"
                name="email"
                label="Email"
                value={inputValues?.email}
                variant="standard"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Iconify icon={"ic:outline-email"} />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => {
                  setUserEdit({ ...inputValues, email: e.target.value });
                }}
                autoComplete="none"
                sx={styleInputFullField}
                error={errors.email ? true : false}
                helperText={
                  errors.email ? (
                    errors.email
                  ) : (
                    <Box component="span" sx={{ color: "white" }}>
                      .
                    </Box>
                  )
                }
                onInput={handleOnInput}
              />
            </Box>
            <Box sx={{ textAlign: "center", mt: 3 }}>
              <TextField
                component="div"
                name="fullname"
                label="Họ Và Tên"
                value={inputValues?.fullname}
                variant="standard"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Iconify icon={"icon-park-solid:edit-name"} />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => {
                  setUserEdit({ ...inputValues, fullname: e.target.value });
                }}
                autoComplete="none"
                sx={styleInputFullField}
                error={errors.fullname ? true : false}
                helperText={
                  errors.fullname ? (
                    errors.fullname
                  ) : (
                    <Box component="span" sx={{ color: "white" }}>
                      .
                    </Box>
                  )
                }
                onInput={handleOnInput}
              />
            </Box>
          </Box>
          <Box textAlign={"right"}>
            <Button type="submit" variant="contained" color="primary">
              Cập nhật tài khoản
            </Button>
          </Box>
        </Box>
      </form>

      <div>
        <Dialog
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle width={400} id="alert-dialog-title">
            <Typography
              component="div"
              sx={{ fontSize: 20 }}
              variant="subtitl1"
            >
              {"Bạn có xác nhận cập nhật tài khoản?"}
            </Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Typography
                component="span"
                sx={{ fontSize: 20, fontFamily: "bold" }}
              >
                <span style={{ color: "red", fontWeight: "bold" }}></span>{" "}
                {inputValues.fullname}
              </Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={updateUser}>Xác nhận</Button>
            <Button onClick={handleCloseDialog} sx={{ color: "red" }} autoFocus>
              Huỷ
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" sx={{ zIndex: 999999 }} />
      </Backdrop>
    </>
  );
}
