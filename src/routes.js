// import { Dashboard } from "@mui/icons-material";
import Dashboard from "./views/dashboard/Default/index";

import MainLayout from "layout/MainLayout";
import { Navigate, useRoutes } from "react-router-dom";
import WorkflowManagement from "views/workflowManagement";
import useLogin from "utils/Login/useLogin";
import LoginPage from "pages/LoginPage/LoginPage";
import Page404 from "views/pages/authentication/Page404";
import WorkDetailsPage from "pages/WorkDetail/WorkDetailsPage";
import ChangePassword from "pages/ProfilePage/ChangePassword"
import UserManagement from "views/userManagement";
import Profile from "pages/ProfilePage/Profile";
import Timekeeping from "views/Timekeeping";
import TimekeepingManagement from "views/TimekeepingManagement";
import LeaveManagement from "views/LeaveManagement";
// layouts

// ----------------------------------------------------------------------

export default function Router() {
  const { account } = useLogin();
  const routes = useRoutes([
    {
      path: "/",
      element: (!account || account.status===false) ? (
        <Navigate to="/login" />
      ) : (
        <Navigate to="/dashboard/default" />
      ),
    },
    {
      path: "/home",
      element: (!account || account.status===false) ? (
        <Navigate to="/login" />
      ) : (
        <Navigate to="/dashboard/default" />
      ),
    },
    {
      path: "/login",
      element: (!account || account.status===false) ? <LoginPage /> : <Navigate to="/dashboard/default" />,
    },
    {
      path: "/",
      element: (!account || account.status===false) ? <Navigate to="/login" /> : <MainLayout />,
      children: [
        {
          path: "/dashboard/default",
          element: <Dashboard />,
        },
        {
          path: "/quanLyCongViec",
          element: <WorkflowManagement />,
        },
        {
          path: "/workDetailsPage/:_id",
          element: <WorkDetailsPage />,
        },
        {
          path: "/forgot-password",
          element: <ChangePassword />,
        },
        {
          path: "/quanLyNguoiDung",
          element: <UserManagement />,
        },
        {
          path: "/profile/:_id",
          element: <Profile />,
        },
        {
          path: "/timekeeping",
          element: <Timekeeping />,
        },
        {
          path: "/managerTimekeeping",
          element: <TimekeepingManagement />,
        },
        {
          path: "/leaveManagement",
          element: <LeaveManagement />,
        },
        
       
      ],
    },
    {
      path: "/404",
      element: <Page404 />,
    },
    {
      path: "*",
      element: <Navigate to="/404" replace />,
    },
  ]);

  return routes;
}
