import { useState, useRef, useEffect } from "react";

// material-ui
import { useTheme } from "@mui/material/styles";
import {
  Avatar,
  Box,
  Chip,
  ClickAwayListener,
  Divider,
  Paper,
  Popper,
  Stack,
  Typography,
  MenuItem,
} from "@mui/material";

// project imports
import MainCard from "ui-component/cards/MainCard";
import Transitions from "ui-component/extended/Transitions";

// assets
import { IconSettings } from "@tabler/icons";
import useLogin from "utils/Login/useLogin";
import Iconify from "ui-component/iconify/Iconify";
import { Link, useNavigate } from "react-router-dom";
import logo from "./../../../../logo.jpg";

// ==============================|| PROFILE MENU ||============================== //

const MENU_OPTIONS = [
  {
    label: "Trang chủ",
    path: "/home",
    icon: "ic:outline-home",
  },
  {
    label: "Đổi mật khẩu",
    path: "/forgot-password",
    icon: "carbon:password",
  },
];

const ProfileSection = () => {
  const { account, logout } = useLogin();

  const navigate = useNavigate();

  const theme = useTheme();

  const [open, setOpen] = useState(false);

  // const [isShowRegister, setShowRegister] = useState(false);

  const anchorRef = useRef(null);

  // const handleClickOpen = () => {
  //   setShowRegister(true);
  // };

  const handleRedirect = (path) => {
    navigate(path);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <>
      <Chip
        sx={{
          height: "48px",
          alignItems: "center",
          borderRadius: "27px",
          transition: "all .2s ease-in-out",
          borderColor: theme.palette.primary.light,
          backgroundColor: theme.palette.primary.light,
          '&[aria-controls="menu-list-grow"], &:hover': {
            borderColor: theme.palette.primary.main,
            background: `${theme.palette.primary.main}!important`,
            color: theme.palette.primary.light,
            "& svg": {
              stroke: theme.palette.primary.light,
            },
          },
          "& .MuiChip-label": {
            lineHeight: 0,
          },
        }}
        icon={
          <Avatar
            src={logo}
            sx={{
              ...theme.typography.mediumAvatar,
              margin: "8px 0 8px 8px !important",
              cursor: "pointer",
            }}
            ref={anchorRef}
            aria-controls={open ? "menu-list-grow" : undefined}
            aria-haspopup="true"
            color="inherit"
          />
        }
        label={
          <IconSettings
            stroke={1.5}
            size="1.5rem"
            color={theme.palette.primary.main}
          />
        }
        variant="outlined"
        ref={anchorRef}
        aria-controls={open ? "menu-list-grow" : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
        color="primary"
      />
      <Popper
        placement="bottom-end"
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        popperOptions={{
          modifiers: [
            {
              name: "offset",
              options: {
                offset: [0, 14],
              },
            },
          ],
        }}
      >
        {({ TransitionProps }) => (
          <Transitions in={open} {...TransitionProps}>
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MainCard
                  border={false}
                  elevation={16}
                  content={false}
                  boxShadow
                  shadow={theme.shadows[16]}
                >
                  <Box sx={{ p: 2 }}>
                    <Stack>
                      <Stack direction="row" spacing={0.5} alignItems="center">
                        <Typography variant="h4">
                          Hi, <span style={{color:'red'}}>{account?.fullname}</span>
                        </Typography>
                        <Typography
                          component="span"
                          variant="h4"
                          sx={{ fontWeight: 400 }}
                        ></Typography>
                      </Stack>
                      <Typography variant="subtitle2">
                        {account?.email}
                      </Typography>
                    </Stack>
                    <Divider />
                  </Box>
                  <Stack sx={{ p: 0.5 }}>
                    {MENU_OPTIONS.map((option) => (
                      <MenuItem sx={{color: "black" }}
                        key={option.label}
                        onClick={() => handleRedirect(option.path)}
                      >
                        <Iconify icon={option.icon} sx={{ marginRight: 1,color: "black"  }} />
                        {option.label}
                      </MenuItem>
                    ))}
                  </Stack>
                  <Link
                    to={`/profile/${account._id}`}
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    <MenuItem sx={{  }}>
                      <Iconify
                        icon={"teenyicons:user-circle-outline"}
                        sx={{ marginRight: 1.3 }}
                      />
                      Thông tin tài khoản
                    </MenuItem>
                  </Link>
                  <Divider />

                  <MenuItem onClick={logout} sx={{ m: 1,color: "black"  }}>
                    <Iconify icon={"bx:log-out"} sx={{ marginRight: 2 }} />
                    Đăng xuất
                  </MenuItem>
                </MainCard>
              </ClickAwayListener>
            </Paper>
          </Transitions>
        )}
      </Popper>
    </>
  );
};

export default ProfileSection;
