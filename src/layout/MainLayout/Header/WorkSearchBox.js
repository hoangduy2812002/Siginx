import {  Divider, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Link } from "react-router-dom";
// import { Link } from "react-router-dom";
// import { toast } from "react-toastify";

export default function WorkSearchBox({ searchData }) {
  const { _id, projectName, contractStatus, investor } = searchData;

  return (
    <>
      <Link
        to={`/workDetailsPage/${_id}`}
        style={{ textDecoration: "none", color: "black" }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            p: 2,
            "&:hover": {
              bgcolor: "#d3f2f2",
              borderColor: "#d3f2f2",
            },
          }}
        >
          <Box display={"flex"} maxWidth="70%">
            <Box sx={{ maxWidth: "99%", ml: 2 }}>
              <Typography
                sx={{ color: "red" }}
                variant="subtitle2"
                noWrap
                fontSize={16}
              >
                {projectName}
              </Typography>

              <Typography variant="body3" noWrap>
                <Box sx={{ color: "blue" }}>
                  {investor} {" - "}( {contractStatus.statusName} )
                </Box>
              </Typography>
            </Box>
          </Box>
        </Box>
      </Link>

      <Divider />
    </>
  );
}
