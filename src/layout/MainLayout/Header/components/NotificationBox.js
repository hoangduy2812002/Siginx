import {
  Avatar,
  Box,
  ListItem,
  ListItemAvatar,
  Typography,
} from "@mui/material";
import DateTimeOfMessage from "utils/DateTimeOfMessage/DateTimeOfMessage";

export default function NotificationBox({ notification }) {

  const { title,content, status, dateTime, avatar } = notification;

  return (
    <ListItem
      button
      alignItems="flex-start"
      sx={
        !status
          ? {
              bgcolor: "#dcf2e7",
              borderBottom: "1px solid blue",
            }
          : { borderBottom: "1px solid blue" }
      }
    >
      <ListItemAvatar>
        <Avatar alt="Profile Picture" src={avatar} />
      </ListItemAvatar>
      <Box>
        <Typography fontWeight={700} fontSize={15}>
          {title}
          {" - "}
          <DateTimeOfMessage dateTime={dateTime} />
        </Typography>
        <Typography fontWeight={400} fontSize={14}>
          {content}
        </Typography>
      </Box>
    </ListItem>
  );
}
