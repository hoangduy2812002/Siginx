import { TextField } from "@mui/material";
import Iconify from "./../../../../ui-component/iconify";

export default function SearchInput({ sx, ...props }) {
  return (
    
    <TextField
      className="rounded"
      size="medium"
      InputProps={{
        endAdornment: (
          <Iconify
            icon="ic:baseline-search"
            width={36}
            sx={{ mr: 1, color: "grey ", }}
          />
        ),
       
      }}
      placeholder="Tìm kiếm dự án..."
      sx={{
        mt:1.5,
        width: "400px",
        "&:hover": {
          "& .MuiInputLabel-root": {
            // color: "red ",
            
          },
          "& .MuiInputBase-root": {
            "& > fieldset": {
              borderColor: "#81abe6",
              
            },
            
          },
        },
        "& .Mui-focused": {
          "&.MuiInputLabel-root": {
            color: "red",
          },
          "&.MuiInputBase-root": {
            "& > fieldset": {
              // borderColor: "red !important",
              
            },
            
          },
        },
        "&.rounded .MuiInputBase-root": {
          paddingRight: 1.5,
          borderRadius:50,
          
        },
        "&.input-24 input": {
          fontSize: "18px",
          
        },
        pr: 1,
        ...sx,
      }}
      {...props}
    />
  );
}
