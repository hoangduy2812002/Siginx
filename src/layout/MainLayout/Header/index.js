import PropTypes from "prop-types";

// material-ui
import { Avatar, Box, ButtonBase, AppBar, Toolbar, Stack } from "@mui/material";

// project imports
import LogoSection from "../LogoSection";
import ProfileSection from "./ProfileSection";
// import SearchPopover from "./SearchPopover";
import { styled } from "@mui/material/styles";
import { bgBlur } from "../../../utils/CssStyle/cssStyles";

// assets
import { IconMenu2 } from "@tabler/icons";
import NotificationPopover from "./NotificationPopover";

const HEADER_MOBILE = 54;

const HEADER_DESKTOP = 72;

const StyledRoot = styled(AppBar)(({ theme }) => ({
  ...bgBlur({ color: theme.palette.background.default }),
  backgroundColor: "#fff",
  boxShadow: "4px 4px 4px #9E9E9E",
}));

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  minHeight: HEADER_MOBILE,
  [theme.breakpoints.up("lg")]: {
    minHeight: HEADER_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}));
// ==============================|| MAIN NAVBAR / HEADER ||============================== //

const notifications = [
  {
    title: "Công việc 1",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 2",
    content: `Ánh lụa đã tạo công việc`,
    avatar:
      "https://doraemon2112shop.com/wp-content/uploads/2020/08/59875579_634137807060090_2576955986480726016_n.jpg",
    isSeen: true,
    dateTime: "13T09:21:21.268Z",
  },
  {
    title: "Công việc 3",
    content: `Hoàng Duy đã tạo công việc`,
    avatar:
      "https://gamesao.vnncdn.net/Resources/Upload/Images/News/471dbf37-bc9f-44bf-aa16-e787c8400b92.jpg",
    isSeen: true,
    dateTime: "13T09:10:56.925Z",
  },
  {
    title: "Công việc 4",
    content: `Vũ Quốc đã tạo công việc`,
    avatar:
      "https://gamesao.vnncdn.net/Resources/Upload/Images/News/471dbf37-bc9f-44bf-aa16-e787c8400b92.jpg",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 5",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 6",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 7",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 7",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 7",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
  {
    title: "Công việc 7",
    content: "Thanh vân đã tạo công việc",
    avatar:
      "https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/451877/item/goods_54_451877.jpg?width=1557&impolicy=quality_75",
    isSeen: false,
    dateTime: "06T07:04:45.313Z",
  },
];

const Header = ({ handleLeftDrawerToggle }) => {

  const handleTouchStart = (event) => {
    console.log("handleTouchStart event", event);
  };
  return (
    <>
      <StyledRoot>
        <StyledToolbar>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
          >
            <Box sx={{ display: "inline-flex" }}>
              <LogoSection />
              <div style={{background:'red'}}>
                <div
                  onMouseDown={handleLeftDrawerToggle}
                  onTouchStart={handleTouchStart}
                >
                  Click or touch me!
                </div>
              </div>
              <ButtonBase sx={{ borderRadius: "12px", overflow: "hidden" }}>
                <Avatar
                  variant="rounded"
                  sx={{
                    "&:hover": {
                      background: "#dee2e3",
                      color: "black",
                    },
                    marginLeft: 8,
                    background: "#e9f0e9",
                    color: "blue",
                  }}
                  onClick={handleLeftDrawerToggle}
                  color="inherit"
                >
                  <IconMenu2 stroke={1.5} size="1.3rem" />
                </Avatar>
              </ButtonBase>
            </Box>

            {/* <SearchPopover /> */}

            <Stack
              direction="row"
              alignItems="center"
              spacing={{
                xs: 0.5,
                sm: 1,
              }}
              sx={{ marginRight: 1 }}
            >
              <Box
                sx={{
                  paddingRight: 3,
                }}
              >
                <NotificationPopover notifications={notifications} />
              </Box>

              <ProfileSection />
            </Stack>
          </Box>
        </StyledToolbar>
      </StyledRoot>
    </>
  );
};

Header.propTypes = {
  handleLeftDrawerToggle: PropTypes.func,
};

export default Header;
