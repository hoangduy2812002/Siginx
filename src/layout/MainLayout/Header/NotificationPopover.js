import { useState } from "react";
// @mui
import {
  Avatar,
  Popover,
  Badge,
  List,
  Divider,
  ListSubheader,
  Button,
  Toolbar,
} from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { Box } from "@mui/system";
import NotificationBox from "./components/NotificationBox";
import { styled } from "@mui/material/styles";

// ----------------------------------------------------------------------

const scrollbar = {
  "::-webkit-scrollbar": {
    width: "8px",
  },
  ":hover::-webkit-scrollbar-thumb": {
    " -webkit-border-radius": "5px",
    borderRadius: "5px",
    background: "#9ae6b1",
  },
  "::-webkit-scrollbar-thumb:window-inactive": {
    background: "#9ae6b1",
  },
};

const HEADER_MOBILE = 64;

const HEADER_DESKTOP = 72;

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  minHeight: HEADER_MOBILE,
  minWidth: 100,
  [theme.breakpoints.up("lg")]: {
    minHeight: HEADER_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}));

export default function NotificationPopover({ notifications, socket }) {
  const [open, setOpen] = useState(false);
  // let count = 0;

  const handleOpen = (event) => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const updateAllNotification = async () => {
    socket.emit("reset_one_user_getAllNotification");
  };

  return (
    <>
      <Badge
        badgeContent={12}
        color="error"
        overlap="circular"
        onClick={handleOpen}
      >
        <Avatar sx={{ bgcolor: "#9ae6b1" }}>
          <NotificationsIcon />
        </Avatar>
      </Badge>

      <Popover
        open={Boolean(open)}
        anchorEl={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        PaperProps={{
          sx: {
            p: 0,
            mt: 1.5,
            ml: 0.75,
            // "& .MuiMenuItem-root": {
            //   typography: "body2",
            //   borderRadius: 0.75,
            // },
            boxShadow:0,
            maxHeight: "82vh",
          //  background:'grey'
          },
        }}
      >
        <StyledToolbar>
          <List
            disablePadding
            sx={{
              width: 500,
              maxHeight: "calc(52vh - 41px)",
              overflow: "auto",
              // ...scrollbar,
            }}
          >
            <Box
              sx={{
                width: 500,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                bgcolor: "#ffffff",
                borderBottom: "1px solid #ed6c02",
                height: 40,
              }}
            >
              <ListSubheader
                component="div"
                id="nested-list-subheader"
                sx={{
                  fontWeight: 700,
                  fontSize: 18,
                  height: 40,
                  lineHeight: "40px",
                }}
              >
                Thông báo
              </ListSubheader>

              <Button
                onClick={updateAllNotification}
                color="error"
                sx={{ mr: 1 }}
              >
                Đánh dấu đã đọc tất cả
              </Button>
            </Box>

            <Divider />
            <List
              disablePadding
              sx={{
                width: 600,
                maxHeight: "calc(52vh - 41px)",
                overflow: "auto",
                ...scrollbar,
              }}
            >
              {notifications?.map((notification, index) => (
                <NotificationBox
                  key={index}
                  notification={notification}
                  socket={socket}
                />
              ))}
            </List>
          </List>
        </StyledToolbar>
      </Popover>
    </>
  );
}
