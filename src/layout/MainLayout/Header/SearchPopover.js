import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { useState } from "react";
import SearchInput from "./components/SearchInput";
import Axios from "utils/Axios";
import WorkSearchBox from "./WorkSearchBox";

const scrollbar = {
  "::-webkit-scrollbar": {
    width: "8px",
  },
  ":hover::-webkit-scrollbar-thumb": {
    " -webkit-border-radius": "5px",
    borderRadius: "5px",
    background: "#dee2e3",
  },
  "::-webkit-scrollbar-thumb:window-inactive": {
    background: "#dee2e3",
  },
};

//

export default function SearchPopover() {
  const [open, setOpen] = useState(false);
  const [isFocusPopup, setFocusPopup] = useState(false);
  const [searchList, setSearchList] = useState([]);
  

  // useEffect(()=>{
  //   searchAllWork();
  // },[])

  const searchAllWork = async ()=>{
    const response = await Axios.Work.getAllWork(0,30,-1,"","");
    if(response.status===200){
      // console.log(response.data.listItem)
      setSearchList(response.data.listItem);
    }
  }

  const handleSearch = async (e) => {
    const response = await Axios.Work.getAllWork(0,100,-1,e.target.value,"");
    setSearchList(response.data.listItem);
  };

  const handleOpen = (event) => {
    searchAllWork();
    setOpen(event.currentTarget);
  };

  const handleClose = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
    }

  };
  const handleClose2 = (event) => {
    if (!isFocusPopup) {
      setOpen(false);
      setFocusPopup(false);
    } else {
      setFocusPopup(false);
    }
  };

  return (
    <Box onFocus={handleOpen} onBlur={handleClose}>
        <SearchInput onInput={handleSearch} />
      <Card
        hidden={!open}
        sx={{
          width: 400,
          position: "fixed",
          mt: 1,
          p: 0,
          borderRadius: 2,
          boxShadow: "0 0 8px #976235",
          maxHeight: "40vh",
        }}
      >
        <Box
          onBlur={handleClose}
          onMouseEnter={() => setFocusPopup(true)}
          onMouseLeave={handleClose2}
        >
          <CardHeader
            title={
              <Typography fontWeight={500} fontSize={16}>
                {"Danh Sách Tìm Kiếm"}
              </Typography>
            }
            sx={{ pb: 1, height: 24 }}
          />
          <Divider />
          <CardContent
            sx={{
              p: "0 !important",
              maxHeight: "calc(40vh - (14*2px))",
              overflow: "auto",
              ...scrollbar,
            }}
          >
            {searchList?.map((searchData, index) => (
              <WorkSearchBox key={index} searchData={searchData} />
            ))}
          </CardContent>
        </Box>
      </Card>
    </Box>
  );
}
