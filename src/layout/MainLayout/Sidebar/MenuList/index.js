// material-ui
import { Typography } from "@mui/material";

// project imports
import NavGroup from "./NavGroup";
import menuItem from "menu-items";
import useLogin from "utils/Login/useLogin";

// ==============================|| SIDEBAR MENU LIST ||============================== //

const MenuList = () => {
  const { account } = useLogin();

  const navItems = menuItem.items.map((item) => {
    switch (item.type) {
      case "group":
        return <NavGroup key={item.id} item={item} />;
      default:
        return (
          <Typography key={item.id} variant="h6" color="error" align="center">
            Menu Items Error
          </Typography>
        );
    }
  });

  if (account.role === "user") {
    navItems[1].props.item.children.splice(1, 1);
  }

  return <>{navItems} </>;
};

export default MenuList;
