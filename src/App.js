
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline, StyledEngineProvider } from '@mui/material';

// routing
import Routes from 'routes';

// defaultTheme
import themes from 'themes';

// project imports
import NavigationScroll from 'layout/NavigationScroll';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


// ==============================|| APP ||============================== //

const App = () => {

    return (

        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={themes("")}>
                <CssBaseline />
                <NavigationScroll>
                    <Routes />
                </NavigationScroll>
                
            </ThemeProvider>
            <ToastContainer autoClose={1000} />

        </StyledEngineProvider>


    );
};

export default App;
