import dashboard from './dashboard';
import utilities from './utilities';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
    // items: [dashboard, pages, utilities, other]
    items: [dashboard, utilities]

};

export default menuItems;
