// assets
import {  IconUsers,IconLayoutGridAdd,IconBrowserCheck,IconDiscountCheck,IconAperture } from '@tabler/icons';

// constant
const icons = {
    IconUsers,
    IconLayoutGridAdd,
    IconBrowserCheck,
    IconDiscountCheck,
    IconAperture
};

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const utilities = {
    id: 'utilities',
    // title: 'Utilities',
    type: 'group',
    children: [
        {
            id: 'managerWork',
            title: 'Quản lý công việc',
            type: 'item',
            url: '/quanLyCongViec',
            icon: icons.IconLayoutGridAdd,
            breadcrumbs: false
        },
        {
            id: 'managerUser',
            title: 'Quản lý người dùng',
            type: 'item',
            url: '/quanLyNguoiDung',
            icon: icons.IconUsers,
            breadcrumbs: false
        },
        {
            id: 'timekeeping',
            title: 'Chấm công',
            type: 'item',
            url: '/timekeeping',
            icon: icons.IconDiscountCheck,
            breadcrumbs: false
        },
        {
            id: 'managerTimekeeping',
            title: 'Quản lý chấm công',
            type: 'item',
            url: '/managerTimekeeping',
            icon: icons.IconBrowserCheck,
            breadcrumbs: false
        },
        {
            id: 'daysOfLeave',
            title: 'Ngày phép',
            type: 'item',
            url: '/leaveManagement',
            icon: icons.IconAperture,
            breadcrumbs: false
        },
    ]
};

export default utilities;
