// material-ui
import {
  Autocomplete,
  Box,
  Container,
  IconButton,
  InputAdornment,
  MenuItem,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  TextField,
  Toolbar,
} from "@mui/material";
import React from "react";
import Iconify from "../../ui-component/iconify";

// import Axios from "utils/Axios";
// import { toast } from "react-toastify";

import { Helmet } from "react-helmet";
import { styled } from "@mui/material/styles";

import { useState } from "react";
import Scrollbar from "ui-component/scrollbar/Scrollbar";
import { TimekeepingListHead } from "sections/@dashboard/timekeeping";

// ==============================|| DEFAULT DASHBOARD ||============================== //
const styleInputFullField = {
  width: "50%",
  mb: 3,
};

const HEADER_MOBILE = 64;

const HEADER_DESKTOP = 72;

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  minHeight: HEADER_MOBILE,
  [theme.breakpoints.up("lg")]: {
    minHeight: HEADER_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}));

const month = [
  { title: "Tháng 1", value: "thang1" },
  { title: "Tháng 2", value: "thang2" },
  { title: "Tháng 3", value: "thang3" },
  { title: "Tháng 4", value: "thang4" },
  { title: "Tháng 5", value: "thang5" },
  { title: "Tháng 6", value: "thang6" },
  { title: "Tháng 7", value: "thang7" },
  { title: "Tháng 8", value: "thang8" },
  { title: "Tháng 9", value: "thang9" },
  { title: "Tháng 10", value: "thang10" },
  { title: "Tháng 11", value: "thang11" },
  { title: "Tháng 12", value: "thang12" },
];

const TABLE_HEAD = [
  { id: "employeeCode", label: "Mã nhân viên", alignRight: false },
  { id: "fullname", label: "Họ tên", alignRight: false },
  { id: "position", label: "Chức vụ", alignRight: false },
  { id: "absent", label: "Vắng", alignRight: false },
  { id: "EnoughWorkingDays", label: "Đủ ngày công", alignRight: false },
  { id: "HalfAWorkingDay", label: "Nửa ngày công", alignRight: false },
  { id: "OnLeave", label: "Nghỉ phép", alignRight: false },
  { id: "StudyAndWork", label: "Học tập, công tác", alignRight: false },
  { id: "Maternity", label: "Thai sản", alignRight: false },
  { id: "NoSalary", label: "Không lương", alignRight: false },
  { id: "TotalWork", label: "Tổng công", alignRight: false },
  { id: "199", label: "", alignRight: false },
];

const listTimekeeping = [
  {
    _id: 1,
    employeeCode: "ID0002",
    fullname: "Phạm Văn Chương",
    absent: "2",
    position: "Software",
    enoughWorkingDays:20,
    halfAWorkingDay:2,
    onLeave:3,
    studyAndWork:1,
    maternity:0,
    noSalary:0,
    totalWork:24,
  },
  {
    _id: 2,
    employeeCode: "ID0005",
    fullname: "Đặng Hoàng Duy",
    absent: "1",
    position: "Software",
    enoughWorkingDays:18,
    halfAWorkingDay:5,
    onLeave:2,
    studyAndWork:3,
    maternity:0,
    noSalary:1,
    totalWork:22,
  },
  {
    _id: 3,
    employeeCode: "ID0003",
    fullname: "Trần Quốc Chí Hải",
    absent: "3",
    position: "Automation",
    enoughWorkingDays:21,
    halfAWorkingDay:1,
    onLeave:5,
    studyAndWork:1,
    maternity:0,
    noSalary:0,
    totalWork:26,
  },
  {
    _id: 4,
    employeeCode: "ID0004",
    fullname: "Lê Trung Hiếu",
    absent: "0",
    position: "Sale",
    enoughWorkingDays:24,
    halfAWorkingDay:4,
    onLeave:2,
    studyAndWork:1,
    maternity:0,
    noSalary:0,
    totalWork:25,
  },
];

const TimekeepingManagement = () => {
  // const { account } = useLogin();
  // const [isEdit, setIsEdit] = useState(false);

  const [open, setOpen] = useState(null);

  const [userEdit, setUserEdit] = useState({
    userId: "",
  });

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleOpenMenu = (event, value) => {
    // setName(value.fullname);
    let arr = [];
    setOpen(event.currentTarget);
    arr.push(value._id);
    // setSelectedOne(arr);
    // setUser(value);
  };

  const handleChangePage = (event, newPage) => {
    // setPage(newPage);
    // getAllWork(newPage, 5, -1, "");
  };

  const handleChangeRowsPerPage = (event) => {
    // getAllWork(0, event.target.value, -1, "");
    // setLimit(parseInt(event.target.value, 10));
  };

  return (
    <>
      <Helmet>
        <title> Trang chủ | Chấm công </title>
      </Helmet>
      <StyledToolbar>
        <Container maxWidth="xl">
          <Autocomplete
            name="major"
            options={month}
            getOptionLabel={(option) => option.title}
            renderInput={(params) => (
              <TextField
                {...params}
                InputProps={{
                  ...params.InputProps,
                  startAdornment: (
                    <InputAdornment position="start">
                      <Iconify icon={"bx:code-block"} />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <React.Fragment>
                      {params.InputProps.endAdornment}
                    </React.Fragment>
                  ),
                }}
                variant="standard"
                label="Chọn tháng"
                placeholder="Chọn tháng"
                onSelect={({ target }) =>
                  setUserEdit((e) => ({
                    ...userEdit,
                    major: target.value,
                  }))
                }
                sx={styleInputFullField}
                // error={errors.major ? true : false}
                // helperText={errors.major}
              />
            )}
          />

          <Box
            sx={{
              width: "102.2%",
              typography: "body1",
              background: "white",
              marginTop: -0.2,
              marginLeft: -2,
            }}
          >
            {/* <UserListToolbar
          numSelected={selected.length}
          selected={selected}
          onChange={getAllUser}
          page={page}
          limit={limit}
        /> */}
            <Scrollbar sx={{ maxHeight: 580 }}>
              <TableContainer
                sx={{
                  minWidth: 800,
                  maxHeight: 560,
                  // ...scrollbar,
                  borderRadius: 2,
                  borderRight: 1,
                  borderLeft: 1,
                  borderBottom: 1,
                  borderColor: "#dee2e3",
                }}
              >
                <Table>
                  <TimekeepingListHead
                    // order={order}
                    // orderBy={orderBy}
                    headLabel={TABLE_HEAD}
                    // rowCount={listUser?.listItem?.length}
                    // numSelected={selected.length}
                    // onRequestSort={handleRequestSort}
                    // onSelectAllClick={handleSelectAllClick}
                    // onChange={getAllUser}
                    // page={page}
                    // limit={limit}
                  />
                  <TableBody>
                    {listTimekeeping?.map((row) => {
                      // totalWork:24,
                      const { _id, employeeCode, fullname, absent, position,enoughWorkingDays,halfAWorkingDay,onLeave,studyAndWork,maternity,noSalary,totalWork } =
                        row;
                      // const selectedWork = selected.indexOf(_id) !== -1;

                      return (
                        <TableRow
                          hover
                          key={_id}
                          tabIndex={-1}
                          role="checkbox"
                          // selected={selectedWork}
                        >
                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            style={{ width: 190, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {employeeCode}
                          </TableCell>
                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            style={{ width: 190, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {fullname}
                          </TableCell>

                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            align="center"
                            style={{ width: 250, whiteSpace: "pre-line" }}
                          >
                            {absent}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {position}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {enoughWorkingDays}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {halfAWorkingDay}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {onLeave}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {studyAndWork}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {maternity}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {noSalary}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {totalWork}
                          </TableCell>

                          <TableCell align="right" sx={{ width: "5%" }}>
                            <IconButton
                              size="large"
                              color="inherit"
                              onClick={(e) => handleOpenMenu(e, row)}
                            >
                              <Iconify icon={"eva:more-vertical-fill"} />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </Scrollbar>
            <TablePagination
              sx={{ mr: 1, mt: -5 }}
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={10}
              rowsPerPage={5}
              page={0}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Box>
          <Popover
            open={Boolean(open)}
            anchorEl={open}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: "top", horizontal: "left" }}
            transformOrigin={{ vertical: "top", horizontal: "right" }}
            PaperProps={{
              sx: {
                p: 1,
                width: 140,
                "& .MuiMenuItem-root": {
                  px: 1,
                  typography: "body2",
                  borderRadius: 0.75,
                },
              },
            }}
          >
            <MenuItem
              sx={{ color: "blue !important" }}
              // onClick={() => setIsEdit(true)}
            >
              <Iconify icon={"eva:edit-fill"} sx={{ mr: 2 }} />
              Chỉnh sửa
            </MenuItem>
          </Popover>
        </Container>
      </StyledToolbar>
    </>
  );
};

export default TimekeepingManagement;
