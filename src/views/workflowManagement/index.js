// @mui
import {
  Table,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Typography,
  IconButton,
  TableContainer,
  Popover,
  MenuItem,
  Box,
  TablePagination,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import React, { useState } from "react";
import { WorkListHead, WorkListToolbar } from "../../sections/@dashboard/work";
import Scrollbar from "../../ui-component/scrollbar";
import Iconify from "../../ui-component/iconify";
import BasicSpeedDial from "./components/BasicSpeedDial";
import { DialogCreateWork } from "./components/DialogCreateWork";
import { DialogEditWork } from "./components/DialogEditWork";
import { Helmet } from "react-helmet";
import { makeStyles } from "@mui/styles";
import Axios from "utils/Axios";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";

const TABLE_HEAD = [
  { id: "projectName", label: "Dự án", alignRight: true },
  { id: "investor", label: "Chủ đầu tư", alignRight: false },
  { id: "contractStatus", label: "Hiện trạng", alignRight: false },
  { id: "technicalProgress", label: "Tiến độ kỹ thuật", alignRight: true },
  { id: "installationProgress", label: "Tiến độ lắp đặt", alignRight: true },
  { id: "processingTime", label: "Thời gian xử lý", alignRight: false },
  { id: "199", label: "", alignRight: true },

  // { id: "processingTime", label: "Hướng xử ", alignRight: false },
];

const scrollbar = {
  "::-webkit-scrollbar": {
    width: "8px",
  },
  ":hover::-webkit-scrollbar-thumb": {
    " -webkit-border-radius": "5px",
    borderRadius: "5px",
    background: "#dee2e3",
  },
  "::-webkit-scrollbar-thumb:window-inactive": {
    background: "#dee2e3",
  },
};

// ----------------------------------------------------------------------
const useStyles = makeStyles({
  table: {
    minWidth: 0,
  },
});

const WorkflowManagement = () => {
  // const navigate = useNavigate();

  const classes = useStyles();
  const [open, setOpen] = useState(null);
  const [showLoading, setShowLoading] = useState(false);

  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(5);

  const [selected, setSelected] = useState([]);

  const [selectedOne, setSelectedOne] = useState([]);

  const [isCreate, setIsCreate] = useState(false);

  const [isEdit, setIsEdit] = useState(false);

  const [listWork, setListWork] = useState([]);

  const [countPage, setCountPage] = useState(0);

  const [project, setProject] = useState({});

  const [order, setOrder] = useState("desc");

  const [orderBy, setOrderBy] = useState("");

  const [openDialog, setOpenDialog] = React.useState(false);

  const [nameWork, setNameWork] = useState("");

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleOpenMenu = (event, value) => {
    setNameWork(value.projectName);
    let arr = [];
    setOpen(event.currentTarget);
    arr.push(value._id);
    setSelectedOne(arr);
    setProject(value);
  };

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = listWork?.listItem?.map((n) => n._id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, _id) => {
    const selectedIndex = selected.indexOf(_id);

    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, _id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getAllWork(newPage, 5, -1, "");
  };

  const handleChangeRowsPerPage = (event) => {
    getAllWork(0, event.target.value, -1, "");
    setLimit(parseInt(event.target.value, 10));
  };

  const handleCreateWork = () => {
    setIsCreate(true);
  };

  const getAllWork = async (page, limit, sortOrder, property) => {
    setShowLoading(() => true);
    const response = await Axios.Work.getAllWork(
      page,
      limit,
      sortOrder,
      "",
      property
    );
    if (response.status === 200) {
      setShowLoading(() => false);
      setSelected([]);
      setPage(page);
      setLimit(limit);
      setListWork(response.data);
      setCountPage(parseInt(response.data.totalItem));
    } else {
      setShowLoading(() => false);
      toast.error("Lấy danh sách dự án thất bại!");
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = ((orderBy === property || orderBy !== property) && order === "asc");
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const deleteWork = async () => {
    setShowLoading(() => true);
    const response = await Axios.Work.deleteWork({ listId: selectedOne });
    if (response.status === 200) {
      setShowLoading(() => false);
      getAllWork(page, limit, order, orderBy);
      setOpenDialog(false);
      setOpen(null);
      toast.success("Xoá dự án thành công!");
    } else {
      setShowLoading(() => false);
      toast.error("Xoá dự án thất bại!");
    }
  };

  return (
    <>
      <Helmet>
        <title> Quản lý công việc </title>
      </Helmet>
      <Box
        sx={{
          width: "102.2%",
          typography: "body1",
          background: "white",
          marginTop: -0.2,
          marginLeft: -2,
        }}
      >
        <WorkListToolbar
          numSelected={selected.length}
          selected={selected}
          onChange={getAllWork}
          page={page}
          limit={limit}
        />
        <Scrollbar sx={{ maxHeight: 580 }}>
          <TableContainer
            sx={{
              minWidth: 10,
              maxHeight: 560,
              ...scrollbar,
              borderRadius: 2,
              borderRight: 1,
              borderLeft: 1,
              borderBottom: 1,
              borderColor: "#dee2e3",
            }}
          >
              
            <Table className={classes.table}>
            <WorkListHead 
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={listWork?.listItem?.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
                onChange={getAllWork}
                page={page}
                limit={limit}
              />
              <TableBody>
                {listWork?.listItem?.map((row) => {
                  const {
                    _id,
                    projectName,
                    investor,
                    contractStatus,
                    processingTime,
                    technicalProgress,
                    installationProgress,
                  } = row;
                  const selectedWork = selected.indexOf(_id) !== -1;

                  return (
                    <TableRow
                      hover
                      key={_id}
                      tabIndex={-1}
                      role="checkbox"
                      selected={selectedWork}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={selectedWork}
                          onChange={(event) => handleClick(event, _id)}
                        />
                      </TableCell>

                      <TableCell
                        align="left"
                        component="th"
                        scope="row"
                        padding="none"
                        style={{ width: 170 }}
                        onClick={(event) => handleClick(event, _id)}
                      >
                        <Typography
                          sx={{ marginLeft: 3, whiteSpace: "pre-line" }}
                        >
                          {projectName}
                        </Typography>
                      </TableCell>

                      <TableCell
                        onClick={(event) => handleClick(event, _id)}
                        style={{ width: 190, whiteSpace: "pre-line" }}
                        align="center"
                      >
                        {investor}
                      </TableCell>

                      <TableCell
                        onClick={(event) => handleClick(event, _id)}
                        align="center"
                        style={{ width: 250, whiteSpace: "pre-line" }}
                      >
                        {contractStatus.statusName}
                      </TableCell>

                      <TableCell
                        onClick={(event) => handleClick(event, _id)}
                        style={{ width: 300, whiteSpace: "pre-line" }}
                        align="left"
                      >
                        {technicalProgress}
                      </TableCell>
                      <TableCell
                        onClick={(event) => handleClick(event, _id)}
                        style={{ width: 250 }}
                        align="left"
                      >
                        {installationProgress}
                      </TableCell>
                      <TableCell
                        onClick={(event) => handleClick(event, _id)}
                        align="center"
                        style={{ whiteSpace: "pre-line" }}
                      >
                        {processingTime}
                      </TableCell>

                      <TableCell align="right" sx={{ width: "5%" }}>
                        <IconButton
                          size="large"
                          color="inherit"
                          onClick={(e) => handleOpenMenu(e, row)}
                        >
                          <Iconify icon={"eva:more-vertical-fill"} />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>
        <TablePagination
          sx={{ mr:10, mt: -5 }}
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={countPage}
          rowsPerPage={limit}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
      <Popover
        open={Boolean(open)}
        anchorEl={open}
        onClose={handleCloseMenu}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        PaperProps={{
          sx: {
            p: 1,
            width: 140,
            "& .MuiMenuItem-root": {
              px: 1,
              typography: "body2",
              borderRadius: 0.75,
            },
          },
        }}
      >
        <Link
          to={`/workDetailsPage/${project._id}`}
          style={{ textDecoration: "none" }}
        >
          <MenuItem sx={{ color: "green !important" }}>
            <Iconify icon={"clarity:details-line"} sx={{ mr: 2 }} />
            Chi tiết
          </MenuItem>
        </Link>

        <MenuItem
          sx={{ color: "blue !important" }}
          onClick={() => setIsEdit(true)}
        >
          <Iconify icon={"eva:edit-fill"} sx={{ mr: 2 }} />
          Chỉnh sửa
        </MenuItem>

        <MenuItem onClick={handleClickOpen} sx={{ color: "red !important" }}>
          <Iconify icon={"eva:trash-2-outline"} sx={{ mr: 2 }} />
          Xoá
        </MenuItem>
      </Popover>

      <DialogCreateWork
        onChange={getAllWork}
        open={isCreate}
        setOpen={setIsCreate}
      />

      <DialogEditWork
        key={1}
        open={isEdit}
        setOpen={setIsEdit}
        project={project}
        getAllWork={getAllWork}
      />
      <BasicSpeedDial handleCreateWork={handleCreateWork} />

      <div>
        <Dialog
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle width={400} id="alert-dialog-title">
            <Typography
              component="div"
              sx={{ fontSize: 20 }}
              variant="subtitl1"
            >
              {"Bạn có xác nhận xoá?"}
            </Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Typography
                component="span"
                sx={{ fontSize: 20, fontFamily: "bold" }}
              >
                <span style={{ color: "red", fontWeight: "bold" }}>
                  {/* {numSelected} */}
                </span>{" "}
                {nameWork}
              </Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={deleteWork}>Xác nhận</Button>
            <Button onClick={handleCloseDialog} sx={{ color: "red" }} autoFocus>
              Huỷ
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};
export default WorkflowManagement;
