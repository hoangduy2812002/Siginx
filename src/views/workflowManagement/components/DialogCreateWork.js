import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import {
  Autocomplete,
  Backdrop,
  Box,
  CircularProgress,
  DialogContentText,
  Divider,
  Grid,
  InputAdornment,
} from "@mui/material";
import Iconify from "../../../ui-component/iconify";
import React, { useEffect, useState } from "react";
import useValidator from "utils/Validator";
import { toast } from "react-toastify";
import Axios from "utils/Axios";
import useLogin from "utils/Login/useLogin";
// import { color } from "@mui/system";

// import useValidator from "../../../utils/Validator";

const styleInputFullField = {
  width: "100%",
  mt: 2,
};

export const DialogCreateWork = ({ open, setOpen, onChange }) => {
  const { validateCreateProject } = useValidator();

  const { account } = useLogin();

  const [workCreate, setWorkCreate] = useState({
    projectName: "",
    intermediary: "",
    technicalProgress: "",
    processingTime: "",
    issue: "",
    investor: "",
    contractStatus: "",
    installationProgress: "",
    solution: "",
    projectStatus: "",
  });

  const [isMultilineName, setIsMultilineName] = useState(false);
  const [isMultilineIntermediary, setIsMultilineIntermediary] = useState(false);
  const [isMultilineProcessingTime, setIsMultilineProcessingTime] =
    useState(false);
  const [isMultilineTechnicalProgres, setIsMultilineTechnicalProgress] =
    useState(false);
  const [isMultilineIssue, setIsMultilineIssue] = useState(false);
  const [isMultilineInvestor, setIsMultilineInvestor] = useState(false);
  const [isMultilineInstallationProgress, setIsMultilineInstallationProgress] =
    useState(false);
  const [isMultilineSolution, setIsMultilineSolution] = useState(false);
  const [isMultilineProjectStatus, setIsMultilineProjectStatus] =
    useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [listContractStatus, setListContractStatus] = useState([]);


  const [errors, setErrors] = useState({});
  const [inputValues, setInputValues] = useState({
    NameOfProject: "",
    Investor: "",
    Clue: "",
    TechnicalProgress: "",
    CurrentStatus: "",
  });

  const handleClose = () => {
    setOpen(false);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    let target = Array.from(e.target);
    let validForm = true;
    let error = {};
    target.forEach((data) => {
      if (data.name) {
        let err = validateCreateProject(data.name, data.value);
        if (err) {
          error[data.name] = err;
          validForm = false;
        }
      }
    });
    setErrors(() => ({ ...errors, ...error }));

    if (validForm) {
      createWork();
      // setShowLoading(() => true);
    } else {
      // console.log("false");
    }
  };

  useEffect(()=>{
    getAllContactStatus();
  },[])

  const getAllContactStatus = async () => {
    setShowLoading(() => true);
    const response = await Axios.Work.getAllContactStatus();
    if (response.status === 200) {
      setListContractStatus(response.data)
      setShowLoading(() => false);
    } else {
      toast.error("Lấy danh sách hiện trạng thất bại!");
      setShowLoading(() => false);
    }
  };

 

  const createWork = async () => {
    setShowLoading(() => true);
    let data = workCreate;
    data.createdBy = account._id;

    const response = await Axios.Work.createWork(data);
    if (response.status === 201) {
      setShowLoading(() => false);
      handleReset();
      toast.success("Tạo dự án thành công");
      setOpen(false);
      onChange(0, 5, -1, "");
    } else {
      setErrors({
        NameOfProject: "Tên dự án đã tồn tại!",
      });
      toast.error("Tạo dự án thất bại!");
      setShowLoading(() => false);
    }
  };

  const handleOnInput = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
    setErrors({ ...errors, [name]: validateCreateProject(name, value) });
  };

  const handleReset = () => {
    setWorkCreate({
      projectName: "",
      intermediary: "",
      technicalProgress: "",
      processingTime: "",
      issue: "",
      investor: "",
      contractStatus: "",
      installationProgress: "",
      solution: "",
      projectStatus: "",
    });
    setErrors({});
  };

  const handleChange = (e, target) => {
    setWorkCreate({
      ...workCreate,
      contractStatus: target?._id,
    });
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose} maxWidth="1000">
        <form onSubmit={handleOnSubmit}>
          <DialogTitle
            sx={{ textAlign: "center", fontSize: 15, color: "blue" }}
          >
            Tạo Dự Án
          </DialogTitle>
          <Divider />
          <DialogContent>
            <DialogContentText />
            <Grid container spacing={2} sx={{ maxWidth: 800 }}>
              <Box tag={"p"} sx={{ mt: 2, display: "flex", width: "100%" }}>
                <Box sx={{ width: "60%", ml: 1 }}>
                  {/* isMultilineName */}
                  {isMultilineName === false ? (
                    <TextField
                      component="div"
                      name="NameOfProject"
                      label="Tên Dự Án"
                      onFocus={() => setIsMultilineName(true)}
                      value={workCreate?.projectName}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          NameOfProject: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tên Dự Án?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.NameOfProject ? true : false}
                      helperText={
                        errors.NameOfProject ? (
                          errors.NameOfProject
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        {
                          ...styleInputFullField,
                          ml: 2,
                          width: "90%",
                          fontFamily: "inherit",
                        })
                      }
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="NameOfProject"
                      label="Tên Dự Án"
                      value={workCreate?.projectName}
                      onBlur={() => setIsMultilineName(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      inputRef={(input) => input && input.focus()}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          projectName: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tên Dự Án?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.NameOfProject ? true : false}
                      helperText={
                        errors.NameOfProject ? (
                          errors.NameOfProject
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        {
                          ...styleInputFullField,
                          ml: 2,
                          width: "90%",
                          fontFamily: "inherit",
                        })
                      }
                      onInput={handleOnInput}
                      autoFocus
                    />
                  )}
                  {/* isMultilineIntermediary */}
                  {isMultilineIntermediary === false ? (
                    <TextField
                      component="div"
                      name="intermediary"
                      label="Đầu Mối"
                      onFocus={() => setIsMultilineIntermediary(true)}
                      value={workCreate?.intermediary}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          intermediary: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tên Đầu Mối?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.intermediary ? true : false}
                      helperText={
                        errors.intermediary ? (
                          errors.intermediary
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        {
                          ...styleInputFullField,
                          ml: 2,
                          width: "90%",
                          fontFamily: "inherit",
                        })
                      }
                      onInput={handleOnInput}
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="intermediary"
                      label="Đầu Mối"
                      onBlur={() => setIsMultilineIntermediary(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      value={workCreate?.intermediary}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          intermediary: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tên Đầu Mối?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.intermediary ? true : false}
                      helperText={
                        errors.intermediary ? (
                          errors.intermediary
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        {
                          ...styleInputFullField,
                          ml: 2,
                          width: "90%",
                          fontFamily: "inherit",
                        })
                      }
                      onInput={handleOnInput}
                      autoFocus
                    />
                  )}
                  {/* isMultilineTechnicalProgres */}
                  {isMultilineTechnicalProgres === false ? (
                    <TextField
                      component="div"
                      name="technicalProgress"
                      label="Tiến Độ Kỹ Thuật"
                      value={workCreate?.technicalProgress}
                      onFocus={() => setIsMultilineTechnicalProgress(true)}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          technicalProgress: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tiến Độ Kỹ Thuật?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.technicalProgress ? true : false}
                      helperText={
                        errors.technicalProgress ? (
                          errors.technicalProgress
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        {
                          ...styleInputFullField,
                          ml: 2,
                          width: "90%",
                          fontFamily: "inherit",
                        })
                      }
                      onInput={handleOnInput}
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="technicalProgress"
                      label="Tiến Độ Kỹ Thuật"
                      value={workCreate?.technicalProgress}
                      onBlur={() => setIsMultilineTechnicalProgress(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          technicalProgress: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tiến Độ Kỹ Thuật?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.technicalProgress ? true : false}
                      helperText={
                        errors.technicalProgress ? (
                          errors.technicalProgress
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        {
                          ...styleInputFullField,
                          ml: 2,
                          width: "90%",
                          fontFamily: "inherit",
                        })
                      }
                      onInput={handleOnInput}
                      autoFocus
                    />
                  )}
                  {/* isMultilineProcessingTime */}
                  {isMultilineProcessingTime === false ? (
                    <TextField
                      component="div"
                      name="processingTime"
                      label="Thời Gian Xử Lý"
                      onFocus={() => setIsMultilineProcessingTime(true)}
                      value={workCreate?.processingTime}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          processingTime: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Thời Gian Xử Lý?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.processingTime ? true : false}
                      helperText={
                        errors.processingTime ? (
                          errors.processingTime
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="processingTime"
                      label="Thời Gian Xử Lý"
                      value={workCreate?.processingTime}
                      onBlur={() => setIsMultilineProcessingTime(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          processingTime: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Thời Gian Xử Lý?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.processingTime ? true : false}
                      helperText={
                        errors.processingTime ? (
                          errors.processingTime
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      autoFocus
                    />
                  )}

                  {/* isMultilineIssue */}
                  {isMultilineIssue === false ? (
                    <TextField
                      component="div"
                      name="issue"
                      label="Tồn Đọng Kỹ Thuật / Thiết Bị"
                      onFocus={() => setIsMultilineIssue(true)}
                      value={workCreate?.issue}
                      onChange={(e) =>
                        setWorkCreate({ ...workCreate, issue: e.target.value })
                      }
                      variant="standard"
                      placeholder="Tồn Đọng?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.issue ? true : false}
                      helperText={
                        errors.issue ? (
                          errors.issue
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="issue"
                      label="Tồn Đọng Kỹ Thuật / Thiết Bị"
                      onBlur={() => setIsMultilineIssue(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      value={workCreate?.issue}
                      onChange={(e) =>
                        setWorkCreate({ ...workCreate, issue: e.target.value })
                      }
                      variant="standard"
                      placeholder="Tồn Đọng?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.issue ? true : false}
                      helperText={
                        errors.issue ? (
                          errors.issue
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      autoFocus
                    />
                  )}
                </Box>

                <Box sx={{ width: "60%" }}>
                  {/* isMultilineInvestor */}
                  {isMultilineInvestor === false ? (
                    <TextField
                      component="div"
                      name="investor"
                      label="Chủ Đầu Tư"
                      onFocus={() => setIsMultilineInvestor(true)}
                      value={workCreate?.investor}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          investor: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Chủ Đầu Tư?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.investor ? true : false}
                      helperText={
                        errors.investor ? (
                          errors.investor
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      onInput={handleOnInput}
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="investor"
                      label="Chủ Đầu Tư"
                      onBlur={() => setIsMultilineInvestor(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      value={workCreate?.investor}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          investor: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Chủ Đầu Tư?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.investor ? true : false}
                      helperText={
                        errors.investor ? (
                          errors.investor
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      onInput={handleOnInput}
                      autoFocus
                    />
                  )}
                  {/* isMultilineContractStatus */}
                  <Autocomplete
                    name="listContractStatus"
                    options={listContractStatus}
                    getOptionLabel={(option) => option.statusName}
                    onChange={handleChange}
                    renderInput={(params) => (
                      <TextField
                        name="contractStatus"
                        {...params}
                        InputProps={{
                          ...params.InputProps,
                          startAdornment: (
                            <InputAdornment position="start">
                              <Iconify icon={"bx:code-block"} />
                            </InputAdornment>
                          ),
                          endAdornment: (
                            <React.Fragment>
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                        variant="standard"
                        label="Hiện trạng"
                        placeholder="Chọn hiện trạng"
                        sx={
                          (styleInputFullField,
                          {
                            ...styleInputFullField,
                            ml: 2,
                            width: "90%",
                          })
                        }
                        error={errors.contractStatus ? true : false}
                        helperText={
                          errors.contractStatus ? (
                            errors.contractStatus
                          ) : (
                            <Box component="span" sx={{ color: "white" }}>
                              .
                            </Box>
                          )
                        }
                      />
                    )}
                  />
                  {/* isMultilineInstallationProgress */}
                  {isMultilineInstallationProgress === false ? (
                    <TextField
                      component="div"
                      name="installationProgress"
                      onFocus={() => setIsMultilineInstallationProgress(true)}
                      value={workCreate?.installationProgress}
                      label="Tiến Độ Lắp Đặt"
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          installationProgress: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tiến Độ?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.installationProgress ? true : false}
                      helperText={
                        errors.installationProgress ? (
                          errors.installationProgress
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="installationProgress"
                      onBlur={() => setIsMultilineInstallationProgress(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      value={workCreate?.installationProgress}
                      label="Tiến Độ Lắp Đặt"
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          installationProgress: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Tiến Độ?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.installationProgress ? true : false}
                      helperText={
                        errors.installationProgress ? (
                          errors.installationProgress
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      autoFocus
                    />
                  )}
                  {/* isMultilineSolution */}
                  {isMultilineSolution === false ? (
                    <TextField
                      component="div"
                      name="solution"
                      label="Hướng Xử Lý"
                      onFocus={() => setIsMultilineSolution(true)}
                      value={workCreate?.solution}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          solution: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Hướng Xử Lý?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.solution ? true : false}
                      helperText={
                        errors.solution ? (
                          errors.solution
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="solution"
                      label="Hướng Xử Lý"
                      onBlur={() => setIsMultilineSolution(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      value={workCreate?.solution}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          solution: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Hướng Xử Lý?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.solution ? true : false}
                      helperText={
                        errors.solution ? (
                          errors.solution
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      autoFocus
                    />
                  )}
                  {/* isMultilineProjectStatus */}
                  {isMultilineProjectStatus === false ? (
                    <TextField
                      component="div"
                      name="projectStatus"
                      label="Giao Hàng - Lắp Đặt"
                      onFocus={() => setIsMultilineProjectStatus(true)}
                      value={workCreate?.projectStatus}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          projectStatus: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Giao Hàng?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.projectStatus ? true : false}
                      helperText={
                        errors.projectStatus ? (
                          errors.projectStatus
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                    />
                  ) : (
                    <TextField
                      component="div"
                      name="projectStatus"
                      label="Giao Hàng - Lắp Đặt"
                      onBlur={() => setIsMultilineProjectStatus(false)}
                      onFocus={(e) =>
                        e.currentTarget.setSelectionRange(
                          e.currentTarget.value.length,
                          e.currentTarget.value.length
                        )
                      }
                      multiline
                      rows={5}
                      value={workCreate?.projectStatus}
                      onChange={(e) =>
                        setWorkCreate({
                          ...workCreate,
                          projectStatus: e.target.value,
                        })
                      }
                      variant="standard"
                      placeholder="Giao Hàng?"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify
                              icon={"material-symbols:edit-note-sharp"}
                            />
                          </InputAdornment>
                        ),
                      }}
                      autoComplete="none"
                      error={errors.projectStatus ? true : false}
                      helperText={
                        errors.projectStatus ? (
                          errors.projectStatus
                        ) : (
                          <Box component="span" sx={{ color: "white" }}>
                            .
                          </Box>
                        )
                      }
                      onInput={handleOnInput}
                      sx={
                        (styleInputFullField,
                        { ...styleInputFullField, ml: 2, width: "90%" })
                      }
                      autoFocus
                    />
                  )}
                </Box>
              </Box>
            </Grid>
          </DialogContent>

          <DialogActions sx={{ p: "0 24px 12px 24px" }}>
            {workCreate.projectName !== "" ||
            workCreate.intermediary !== "" ||
            workCreate.technicalProgress !== "" ||
            workCreate.processingTime !== "" ||
            workCreate.issue !== "" ||
            workCreate.investor !== "" ||
            workCreate.contractStatus !== "" ||
            workCreate.installationProgress ||
            workCreate.solution ||
            workCreate.projectStatus !== "" ||
            Object.keys(errors).length !== 0 ? (
              <Button
                onClick={handleReset}
                variant="contained"
                sx={{ background: "#f78b25" }}
              >
                Làm mới
              </Button>
            ) : (
              ""
            )}

            <Button
              type="submit"
              variant="contained"
              sx={{ background: "#2579f7" }}
            >
              Tạo Dự Án
            </Button>
            <Button
              onClick={handleClose}
              variant="contained"
              sx={{ background: "#f72533" }}
            >
              Hủy
            </Button>
          </DialogActions>
        </form>
      </Dialog>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};
