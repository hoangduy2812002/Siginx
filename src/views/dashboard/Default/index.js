// material-ui
import { Grid, Container, Backdrop, CircularProgress } from "@mui/material";
// sections
import AppWidgetSummary from "../../../sections/@dashboard/app/AppWidgetSummary";
import AppNewsUpdate from "../../../sections/@dashboard/app/AppNewsUpdate";
import Axios from "utils/Axios";
import { toast } from "react-toastify";

import { Helmet } from "react-helmet";
import { useEffect, useState } from "react";
import useLogin from "utils/Login/useLogin";

// ==============================|| DEFAULT DASHBOARD ||============================== //

const Dashboard = () => {
  const { account } = useLogin();
  const [totalProject, setTotalProject] = useState(0);
  const [totalUser, setTotalUser] = useState(0);
  const [totalProcessingProject, setTotalProcessingProject] = useState(0);
  const [totalSignedContractProject, setTotalSignedContractProject] =
    useState(0);

  const [listProject, setListProject] = useState([]);
  const [showLoading, setShowLoading] = useState(false);

  useEffect(() => {
    getAllWork();
    getAllStatistical();
  }, []);

  const getAllWork = async () => {
    setShowLoading(() => true);
    const response = await Axios.Work.getAllWork(0, 3, -1, "", "");
    if (response.status === 200) {
      setShowLoading(() => false);
      setListProject(response.data.listItem);
    } else {
      setShowLoading(() => false);
      toast.error("Lấy danh sách dự án thất bại!");
    }
  };

  const getAllStatistical = async () => {
    setShowLoading(() => true);
    const response = await Axios.Statistical.getAllStatistical();
    if (response.status === 200) {
      setShowLoading(() => false);
      setTotalUser(response.data.totalUser);
      setTotalProject(response.data.totalProject);
      setTotalProcessingProject(response.data.processingProject);
      setTotalSignedContractProject(response.data.signedContractProject);
    } else {
      setShowLoading(() => false);
      toast.error("Lấy danh sách thống kê thất bại!");
    }
  };

  return (
    <>
      <Helmet>
        <title> Trang chủ | Dashboard </title>
      </Helmet>
      <Container maxWidth="xl">
        <Grid container spacing={3}>
          {account.role === "admin" ? (
            <Grid item xs={12} sm={6} md={3}>
              <AppWidgetSummary
                title="Người dùng"
                total={totalUser}
                icon={"mdi:user-group-outline"}
                sx={{ bgcolor: "#E0F8F7" }}
              />
            </Grid>
          ) : (
            ""
          )}

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              title="Dự án"
              total={totalProject}
              color="success"
              icon={"la:project-diagram"}
              sx={{ bgcolor: "#D8F6CE" }}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              color="error"
              title="Đang xử lý"
              total={totalProcessingProject}
              icon={"fluent-mdl2:processing-run"}
              sx={{ bgcolor: "#FBEFEF" }}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              title="Đã ký hợp đồng"
              total={totalSignedContractProject}
              color="warning"
              icon={"material-symbols:error-med-outline"}
              sx={{ bgcolor: "#F7F8E0" }}
            />
          </Grid>

          <Grid item sx={{ minWidth: 450, width: 1600 }}>
            <AppNewsUpdate title="Công việc mới cập nhật" list={listProject} />
          </Grid>
        </Grid>
      </Container>

      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" sx={{ zIndex: 999999 }} />
      </Backdrop>
    </>
  );
};

export default Dashboard;
