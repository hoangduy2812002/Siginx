// material-ui
import {
  Autocomplete,
  Box,
  Container,
  IconButton,
  InputAdornment,
  MenuItem,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  TextField,
  Toolbar,
} from "@mui/material";
import React from "react";
import Iconify from "../../ui-component/iconify";

// import Axios from "utils/Axios";
// import { toast } from "react-toastify";

import { Helmet } from "react-helmet";
import { styled } from "@mui/material/styles";

import { useState } from "react";
import Scrollbar from "ui-component/scrollbar/Scrollbar";
import { TimekeepingListHead } from "sections/@dashboard/timekeeping";

// ==============================|| DEFAULT DASHBOARD ||============================== //
const styleInputFullField = {
  width: "50%",
  mb: 3,
};

const HEADER_MOBILE = 64;

const HEADER_DESKTOP = 72;

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  minHeight: HEADER_MOBILE,
  [theme.breakpoints.up("lg")]: {
    minHeight: HEADER_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}));

const year = [
  { title: "Năm 2022", value: "year2022" },
  { title: "Năm 2023", value: "year2023" },
  { title: "Năm 2024", value: "year2024" },
];

const TABLE_HEAD = [
  { id: "employeeCode", label: "Mã nhân viên", alignRight: false },
  { id: "fullname", label: "Họ tên", alignRight: false },
  { id: "position", label: "Chức vụ", alignRight: false },
  { id: "totalLeaveDays", label: "Tổng ngày phép", alignRight: false },
  { id: "halfWorkingDay", label: "Nửa ngày công", alignRight: false },
  { id: "vacation", label: "Nghỉ phép", alignRight: false },
  { id: "studyAndWork", label: "Học tập, công tác", alignRight: false },
  { id: "maternity", label: "Thai sản", alignRight: false },
  { id: "noSalary", label: "Không lương", alignRight: false },
  { id: "remote", label: "Remote", alignRight: false },
  { id: "remainingLeaveDays", label: "Số ngày phép còn", alignRight: false },
  
  { id: "12", label: "", alignRight: false },
];

const listTimekeeping = [
  {
    _id: 1,
    employeeCode: "ID0002",
    fullname: "Phạm Văn Chương",
    enoughWorkingDays: "Software",
    totalLeaveDays:7,
    halfWorkingDay: 1,
    vacation: 3,
    studyAndWork:2,
    maternity:0,
    noSalary:0,
    remote:7,
    remainingLeaveDays:3
  },
  {
    _id: 2,
    employeeCode: "ID0005",
    fullname: "Đặng Hoàng Duy",
    enoughWorkingDays: "Software",
    totalLeaveDays:5,
    halfWorkingDay: 2,
    vacation: 2,
    studyAndWork:3,
    maternity:0,
    noSalary:0,
    remote:2,
    remainingLeaveDays:6
  },
  {
    _id: 3,
    employeeCode: "ID0003",
    fullname: "Trương Quốc Chí Hải",
    enoughWorkingDays: "Automation",
    totalLeaveDays:8,
    halfWorkingDay: 2,
    vacation: 2,
    studyAndWork:3,
    maternity:0,
    noSalary:0,
    remote:1,
    remainingLeaveDays:8
  },
  {
    _id: 4,
    employeeCode: "ID0004",
    fullname: "Lê Trung Hiếu",
    enoughWorkingDays: "Sale",
    totalLeaveDays:9,
    halfWorkingDay: 2,
    vacation: 2,
    studyAndWork:3,
    maternity:0,
    noSalary:0,
    remote:1,
    remainingLeaveDays:2
  },
];

const LeaveManagement = () => {
  // const { account } = useLogin();
  // const [isEdit, setIsEdit] = useState(false);

  const [open, setOpen] = useState(null);

  const [userEdit, setUserEdit] = useState({
    userId: "",
  });

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleOpenMenu = (event, value) => {
    // setName(value.fullname);
    let arr = [];
    setOpen(event.currentTarget);
    arr.push(value._id);
    // setSelectedOne(arr);
    // setUser(value);
  };

  const handleChangePage = (event, newPage) => {
    // setPage(newPage);
    // getAllWork(newPage, 5, -1, "");
  };

  const handleChangeRowsPerPage = (event) => {
    // getAllWork(0, event.target.value, -1, "");
    // setLimit(parseInt(event.target.value, 10));
  };

  return (
    <>
      <Helmet>
        <title> Trang chủ | Chấm công </title>
      </Helmet>
      <StyledToolbar>
        <Container maxWidth="xl">
          <Autocomplete
            name="major"
            options={year}
            getOptionLabel={(option) => option.title}
            renderInput={(params) => (
              <TextField
                {...params}
                InputProps={{
                  ...params.InputProps,
                  startAdornment: (
                    <InputAdornment position="start">
                      <Iconify icon={"bx:code-block"} />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <React.Fragment>
                      {params.InputProps.endAdornment}
                    </React.Fragment>
                  ),
                }}
                variant="standard"
                label="Chọn năm"
                placeholder="Chọn năm"
                onSelect={({ target }) =>
                  setUserEdit((e) => ({
                    ...userEdit,
                    major: target.value,
                  }))
                }
                sx={styleInputFullField}
                // error={errors.major ? true : false}
                // helperText={errors.major}
              />
            )}
          />

          <Box
            sx={{
              width: "102.2%",
              typography: "body1",
              background: "white",
              marginTop: -0.2,
              marginLeft: -2,
            }}
          >
            {/* <UserListToolbar
          numSelected={selected.length}
          selected={selected}
          onChange={getAllUser}
          page={page}
          limit={limit}
        /> */}
            <Scrollbar sx={{ maxHeight: 580 }}>
              <TableContainer
                sx={{
                  minWidth: 800,
                  maxHeight: 560,
                  // ...scrollbar,
                  borderRadius: 2,
                  borderRight: 1,
                  borderLeft: 1,
                  borderBottom: 1,
                  borderColor: "#dee2e3",
                }}
              >
                <Table>
                  <TimekeepingListHead
                    // order={order}
                    // orderBy={orderBy}
                    headLabel={TABLE_HEAD}
                    // rowCount={listUser?.listItem?.length}
                    // numSelected={selected.length}
                    // onRequestSort={handleRequestSort}
                    // onSelectAllClick={handleSelectAllClick}
                    // onChange={getAllUser}
                    // page={page}
                    // limit={limit}
                  />
                  <TableBody>
                    {listTimekeeping?.map((row) => {
                      const { _id, employeeCode, fullname,enoughWorkingDays,totalLeaveDays,halfWorkingDay,vacation,studyAndWork,maternity,noSalary,remote,remainingLeaveDays } =
                        row;
                      // const selectedWork = selected.indexOf(_id) !== -1;

                      return (
                        <TableRow
                          hover
                          key={_id}
                          tabIndex={-1}
                          role="checkbox"
                          // selected={selectedWork}
                        >
                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            style={{ width: 190, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {employeeCode}
                          </TableCell>

                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            align="center"
                            style={{ width: 250, whiteSpace: "pre-line" }}
                          >
                            {fullname}
                          </TableCell>

                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {enoughWorkingDays}
                          </TableCell>

                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {totalLeaveDays}
                          </TableCell>

                          <TableCell
                            // onClick={(event) => handleClick(event, _id)}
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {halfWorkingDay}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {vacation}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {studyAndWork}
                          </TableCell>


                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {maternity}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {noSalary}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {remote}
                          </TableCell>

                          <TableCell
                            style={{ width: 300, whiteSpace: "pre-line" }}
                            align="center"
                          >
                            {remainingLeaveDays}
                          </TableCell>

                          <TableCell align="right" sx={{ width: "5%" }}>
                            <IconButton
                              size="large"
                              color="inherit"
                              onClick={(e) => handleOpenMenu(e, row)}
                            >
                              <Iconify icon={"eva:more-vertical-fill"} />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </Scrollbar>
            <TablePagination
              sx={{ mr: 1, mt: -5 }}
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={10}
              rowsPerPage={5}
              page={0}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Box>
          <Popover
            open={Boolean(open)}
            anchorEl={open}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: "top", horizontal: "left" }}
            transformOrigin={{ vertical: "top", horizontal: "right" }}
            PaperProps={{
              sx: {
                p: 1,
                width: 140,
                "& .MuiMenuItem-root": {
                  px: 1,
                  typography: "body2",
                  borderRadius: 0.75,
                },
              },
            }}
          >
            <MenuItem
              sx={{ color: "blue !important" }}
              // onClick={() => setIsEdit(true)}
            >
              <Iconify icon={"eva:edit-fill"} sx={{ mr: 2 }} />
              Chỉnh sửa
            </MenuItem>
          </Popover>
        </Container>
      </StyledToolbar>
      {/* <TimekeepingDialog key={1} open={isEdit} setOpen={setIsEdit} /> */}
    </>
  );
};

export default LeaveManagement;
