import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import {
  Backdrop,
  Box,
  CircularProgress,
  DialogContentText,
  Divider,
  Grid,
  InputAdornment,
  Typography,
} from "@mui/material";
import Iconify from "../../../ui-component/iconify";
import { useState } from "react";
import { toast } from "react-toastify";
import useValidator from "utils/Validator";
import { useEffect } from "react";
import Axios from "utils/Axios";

// import useValidator from "../../../utils/Validator";

const styleInputFullField = {
  width: "100%",
  mb: 3,
  ml: 2,
};

const LayoutFormTwoField = ({ children }) => {
  return (
    <Grid container spacing={2} sx={{ width: "100%" }}>
      {children}
    </Grid>
  );
};

export const DialogEditUser = ({ open, setOpen, user, onChange }) => {
  const { validate } = useValidator();

  const [userEdit, setUserEdit] = useState({});
  const [openDialog, setOpenDialog] = React.useState(false);
  const [openDialogResetPassword, setOpenDialogResetPassword] =
    React.useState(false);

  const [showLoading, setShowLoading] = useState(false);

  let notePassword = "Password reset ==> ";
  let newPass = "12345";

  useEffect(() => {
    setUserEdit(user);
  }, [user]);

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleCloseDialogResetPassword = () => {
    setOpenDialogResetPassword(false);
  };

  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleClickOpenResetPassword = () => {
    setOpenDialogResetPassword(true);
  };

  const updateUser = async () => {
    setShowLoading(() => true);
    const response = await Axios.Profile.updateUser(userEdit._id, userEdit);
    if (response.status === 200) {
      setShowLoading(() => false);
      setOpenDialog(false);
      toast.success("Cập nhật người dùng thành công");
      setOpen(false);
      onChange(0, 5, -1, "");
    } else {
      setShowLoading(() => false);
      setOpenDialog(false);
      setErrors({
        email: "Email đã tồn tại!",
      });
      // toast.error("Cập nhật người dùng thất bại!");
    }
  };

  const resetPassword = async () => {
    setShowLoading(() => true);
    const response = await Axios.Profile.resetPassword({ id: userEdit._id });
    if (response.status === 200) {
      setShowLoading(() => false);
      setOpenDialogResetPassword(false);

      toast.success("Reset password thành công");
      setOpen(false);
    } else {
      setShowLoading(() => false);
      toast.error("Reset password thất bại!");
    }
  };

  const [errors, setErrors] = useState({
    email: "",
    fullname: "",
  });
  const [inputValues, setInputValues] = useState({
    email: "",
    fullname: "",
  });

  const handleClose = () => {
    setOpen(false);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    let target = Array.from(e.target);
    let validForm = true;
    let error = {};
    target.forEach((data) => {
      if (data.name) {
        let err = validate(data.name, data.value);
        if (err) {
          error[data.name] = err;
          validForm = false;
        }
      }
    });
    setErrors(() => ({ ...errors, ...error }));

    if (validForm) {
      handleClickOpen();
      // setShowLoading(() => true);
    } else {
      toast.error("Vui lòng điền đầy đủ thông tin!");
    }
  };

  const handleOnInput = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
    setErrors({ ...errors, [name]: validate(name, value) });
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose} maxWidth="1000">
        <form onSubmit={handleOnSubmit}>
          <DialogTitle>Chỉnh sửa tài khoản</DialogTitle>
          <Divider />
          <DialogContent>
            <DialogContentText />
            <Grid container spacing={2} sx={{ maxWidth: 800 }}>
              <LayoutFormTwoField>
                <Grid item xs={6}>
                  <TextField
                    component="div"
                    name="email"
                    label="Email"
                    value={userEdit?.email}
                    variant="standard"
                    placeholder="Nhập email"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <Iconify icon={"entypo:mail"} />
                        </InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setUserEdit({ ...userEdit, email: e.target.value });
                    }}
                    autoComplete="none"
                    sx={styleInputFullField}
                    error={errors.email ? true : false}
                    helperText={
                      errors.email ? (
                        errors.email
                      ) : (
                        <Box component="span" sx={{ color: "white" }}>
                          .
                        </Box>
                      )
                    }
                    onInput={handleOnInput}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    name="fullname"
                    label="Họ Và Tên"
                    value={userEdit?.fullname}
                    placeholder="Nhập Họ Và Tên"
                    variant="standard"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <Iconify icon={"icon-park-solid:edit-name"} />
                        </InputAdornment>
                      ),
                    }}
                    onChange={(e) => {
                      setUserEdit({ ...userEdit, fullname: e.target.value });
                    }}
                    autoComplete="none"
                    sx={styleInputFullField}
                    error={errors.fullname ? true : false}
                    helperText={
                      errors.fullname ? (
                        errors.fullname
                      ) : (
                        <Box component="span" sx={{ color: "white" }}>
                          .
                        </Box>
                      )
                    }
                    onInput={handleOnInput}
                  />
                </Grid>
              </LayoutFormTwoField>
              <LayoutFormTwoField >
                <Grid item xs={6} >
                </Grid>
                <Grid item xs={6}>
                  {/* <TextField /> */}
                  <Box sx={{background:'white',ml:20,color:'white'}}>.</Box>
                </Grid>
              </LayoutFormTwoField>
            </Grid>
          </DialogContent>
          <DialogActions sx={{ p: "0 24px 12px 24px" }}>
            <Button
              onClick={handleClickOpenResetPassword}
              variant="contained"
              sx={{ background: "green" }}
            >
              Reset password
            </Button>
            <Button type="submit" variant="contained">
              Cập nhật
            </Button>
            <Button
              onClick={handleClose}
              variant="contained"
              sx={{ background: "red" }}
            >
              Hủy
            </Button>
          </DialogActions>
        </form>
      </Dialog>

      <div>
        <Dialog
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle width={400} id="alert-dialog-title">
            <Typography
              component="div"
              sx={{ fontSize: 20 }}
              variant="subtitl1"
            >
              {"Bạn có xác nhận cập nhật tài khoản?"}
            </Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Typography
                component="span"
                sx={{ fontSize: 20, fontFamily: "bold" }}
              >
                <span style={{ color: "red", fontWeight: "bold" }}></span>{" "}
                {inputValues.fullname}
              </Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={updateUser}>Xác nhận</Button>
            <Button onClick={handleCloseDialog} sx={{ color: "red" }} autoFocus>
              Huỷ
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      {/* resetPassword */}
      <div>
        <Dialog
          open={openDialogResetPassword}
          onClose={handleCloseDialogResetPassword}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle width={400} id="alert-dialog-title">
            <Typography
              component="div"
              sx={{ fontSize: 20 }}
              variant="subtitl1"
            >
              {"Bạn có xác nhận reset password?"}
            </Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Typography
                component="span"
                sx={{ fontSize: 20, fontFamily: "bold" }}
              >
                <span style={{ color: "black", fontWeight: "bold" }}>
                  {notePassword}{" "}
                </span>
                <span style={{ color: "red", fontWeight: "bold" }}>
                  {" "}
                  {newPass}
                </span>
              </Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={resetPassword}>Xác nhận</Button>
            <Button
              onClick={handleCloseDialogResetPassword}
              sx={{ color: "red" }}
              autoFocus
            >
              Huỷ
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" sx={{ zIndex: 999999 }} />
      </Backdrop>
    </>
  );
};
