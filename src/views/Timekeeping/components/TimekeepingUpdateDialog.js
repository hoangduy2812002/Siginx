import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import {
  Autocomplete,
  Backdrop,
  CircularProgress,
  DialogContentText,
  Divider,
  Grid,
  InputAdornment,
  Typography,
} from "@mui/material";
import Iconify from "ui-component/iconify";
import Axios from "utils/Axios";
import { toast } from "react-toastify";
import useValidator from "utils/Validator";
import DateTimeOfDay from "utils/DateTimeOfMessage/DateTimeOfDay";

const styleInputFullField = {
  width: "100%",
  mb: 3,
  ml: 2,
};

const content = [
  { content: "Đủ ngày công", value: "X" },
  { content: "Nửa ngày công", value: "X/2" },
  { content: "Nghỉ phép", value: "P" },
  { content: "Học tập, công tác", value: "H" },
  { content: "Thai sản", value: "ThS" },
  { content: "Không lương", value: "KL" },
  { content: "Remote", value: "R" },
];
let arr = [];
content.forEach((element) => {
  arr.push(element.content);
});

const LayoutFormTwoField = ({ children }) => {
  return (
    <Grid container spacing={2} sx={{ width: "100%" }}>
      {children}
    </Grid>
  );
};

export const TimekeepingUpdateDialog = ({
  open = false,
  setOpen,
  timekeeping,
  onChange,
  month,
}) => {
  const { validateTimeKeeping } = useValidator();

  const [timekeepingReg, setTimekeepingReg] = useState({
    date: "",
    content: timekeeping.content,
    note: "",
  });

  const [showLoading, setShowLoading] = useState(false);

  const [inputValues, setInputValues] = useState({
    date: "",
    content: "",
    note: "",
  });

  const [openDialog, setOpenDialog] = React.useState(false);


  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const [errors, setErrors] = useState({
    date: "",
    content: "",
    note: "",
  });

  useEffect(() => {
    setErrors({
      content: "",
    });
    setTimekeepingReg({
      date: "",
      content: timekeeping.content,
      note: timekeeping.note,
    });
  }, [timekeeping]);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    let target = Array.from(e.target);
    let validForm = true;
    let error = {};
    target.forEach((data) => {
      if (data.name) {
        let err = validateTimeKeeping(data.name, data.value);
        if (err) {
          error[data.name] = err;
          validForm = false;
        }
      }
    });
    setErrors(() => ({ ...errors, ...error }));

    if (validForm) {
      attenDance();
    } else {
      console.log("false");
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOnInput = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
    setErrors({ ...errors, [name]: validateTimeKeeping(name, value) });
  };

  const attenDance = async () => {
    setShowLoading(() => true);
    timekeepingReg.date = timekeeping.start;
    const response = await Axios.Timekeeping.attenDance(timekeepingReg);
    if (response.status === 200) {
      onChange(month);
      toast.success("Điểm danh thành công");
      setOpen(false);
      setShowLoading(() => false);
    } else {
      setShowLoading(() => false);
      toast.error("Điểm danh thất bại!");
    }
  };

  const deleteTimeKeeping = async () => {
    setShowLoading(() => true);
    timekeepingReg.date = timekeeping.start;
    const response = await Axios.Timekeeping.deleteTimeKeeping(timekeeping._id);
    if (response.status === 200) {
      onChange(month);
      setOpenDialog(false);
      toast.success("Xoá điểm danh thành công");
      setOpen(false);
      setShowLoading(() => false);
    } else {
      setShowLoading(() => false);
      toast.error("Xoá điểm danh thất bại!");
    }
  };

  const handleChange = (e, target) => {
    setTimekeepingReg({
      ...timekeepingReg,
      content: target?.content,
    });
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose} maxWidth="1000">
        <form onSubmit={handleOnSubmit}>
          <DialogTitle>
            Cập nhật chấm công - ({" "}
            <DateTimeOfDay dateTime={timekeeping.start} /> )
          </DialogTitle>
          <Divider />
          <DialogContent sx={{ maxWidth: 450 }}>
            <DialogContentText />
            {/* arr.) */}
            <Grid container spacing={2}>
              <LayoutFormTwoField>
                <Autocomplete
                  name="listcontent"
                  options={content}
                  defaultValue={content[arr.indexOf(timekeeping.title)]}
                  getOptionLabel={(option) => option?.content}
                  onChange={handleChange}
                  renderInput={(params) => (
                    <TextField
                      name="content"
                      {...params}
                      InputProps={{
                        ...params.InputProps,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Iconify icon={"bx:code-block"} />
                          </InputAdornment>
                        ),
                        endAdornment: (
                          <React.Fragment>
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                      variant="standard"
                      label="Nội dung"
                      placeholder="Chọn nội dung"
                      sx={{
                        styleInputFullField,
                        ...styleInputFullField,
                        width: 240,
                        ml: 4,
                        mt: 3,
                      }}
                      error={errors.content ? true : false}
                      helperText={errors.content}
                    />
                  )}
                />
                <Button
                  variant="contained"
                  onClick={handleClickOpen}
                  sx={{
                    background: "#56d446",
                    height: 30,
                    marginTop: 4.7,
                    position: "absolute",
                    ml: 36,
                  }}
                >
                  Xoá điểm danh
                </Button>
                {/* </Grid> */}
              </LayoutFormTwoField>
              <TextField
                // name="note"
                label="Ghi chú"
                placeholder="Nhập Ghi Chú"
                value={timekeepingReg?.note ? timekeepingReg?.note : ""}
                variant="standard"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Iconify icon={"material-symbols:edit-note-sharp"} />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => {
                  setTimekeepingReg({
                    ...timekeepingReg,
                    note: e.target.value,
                  });
                }}
                autoComplete="none"
                sx={{
                  styleInputFullField,
                  ...styleInputFullField,
                }}
                onInput={handleOnInput}
              />
            </Grid>
          </DialogContent>
          <DialogActions sx={{ p: "0 24px 12px 24px" }}>
            <Button type="submit" variant="contained">
              Cập nhật
            </Button>
            <Button
              onClick={handleClose}
              variant="contained"
              sx={{ background: "red" }}
            >
              Hủy
            </Button>
          </DialogActions>
        </form>
      </Dialog>

      <div>
        <Dialog
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle width={400} id="alert-dialog-title">
            <Typography
              component="div"
              sx={{ fontSize: 20 }}
              variant="subtitl1"
            >
              {"Bạn có xác nhận xoá?"}
            </Typography>
          </DialogTitle>
          <DialogContent>
            {/* <DialogContentText id="alert-dialog-description">
              <Typography
                component="span"
                sx={{ fontSize: 20, fontFamily: "bold" }}
              >
              </Typography>
            </DialogContentText> */}
          </DialogContent>
          <DialogActions>
            <Button onClick={deleteTimeKeeping}>Xác nhận</Button>
            <Button onClick={handleCloseDialog} sx={{ color: "red" }} autoFocus>
              Huỷ
            </Button>
          </DialogActions>
        </Dialog>
      </div>

      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" sx={{ zIndex: 999999 }} />
      </Backdrop>
    </>
  );
};
