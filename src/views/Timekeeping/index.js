import React, { useState, useEffect } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Axios from "utils/Axios";
import { toast } from "react-toastify";

import { Helmet } from "react-helmet";

import { TimekeepingDialog } from "./components/TimekeepingDialog";
import { Backdrop, CircularProgress } from "@mui/material";
import { TimekeepingUpdateDialog } from "./components/TimekeepingUpdateDialog";
import Iconify from "ui-component/iconify";
import "./index.scss";

const localizer = momentLocalizer(moment);

const Timekeeping = () => {
  const [isEdit, setIsEdit] = useState(false);
  const [isEditUpdate, setIsEditUpdate] = useState(false);
  const [timekeeping, setTimekeeping] = useState({});
  const [timekeepingUpdate, setTimekeepingUpdate] = useState({});

  const [listEvent, setListEvent] = useState([]);
  const [showLoading, setShowLoading] = useState(false);
  const [month, setMonth] = useState(getMonthFromStringToday(new Date()));


  const handleSelected = (event) => {
    setTimekeeping(event);
    setIsEdit(true);
  };

  useEffect(() => {
    // console.log()
    getAllTimekeeping(month);
  }, [month]);

  const getAllTimekeeping = async (data) => {
    setShowLoading(() => true);
    let arr = [];
    const response = await Axios.Timekeeping.getAllTimekeeping(data);
    if (response.status === 200) {
      setShowLoading(() => false);
      response.data.forEach((element) => {
        let listEventObj = {};
        listEventObj._id = element._id;
        listEventObj.title = element.content;
        listEventObj.start = element.date;
        listEventObj.end = element.date;
        listEventObj.note = element.note;
        arr.push(listEventObj);
      });
    } else {
      toast.error("Lấy danh sách điểm danh thất bại!");
      setShowLoading(() => false);
    }
    setListEvent(arr);
  };

  function getMonthFromStringNext(dateString) {
    var date = new Date(dateString);
    let month = date.getMonth() + 2;
    let year = date.getFullYear();
    if (month > 12) {
      year = year + 1;
      month = 1;
    }
    var result = year + "-" + month;
    setMonth(result);
    return result;
  }

  function getMonthFromStringPrev(dateString) {
    var date = new Date(dateString);
    let month = date.getMonth() - 0;
    let year = date.getFullYear();
    if (month < 1) {
      year = year - 1;
      month = 12;
    }
    var result = year + "-" + month;
    setMonth(result);
    return result;
  }

  function getMonthFromStringToday(dateString) {
    var date = new Date(dateString);
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    if (month > 12) {
      year = year + 1;
      month = 1;
    }
    var result = year + "-" + month;
    return result;
  }

  const prev = (props) => {
    getMonthFromStringPrev(props);
  };
  const next = (props) => {
    getMonthFromStringNext(props);
  };
  const toDay = () => {
    getMonthFromStringToday(new Date());
    setMonth(getMonthFromStringToday(new Date()));
  };

  function getMonthLabel(dateString) {
    var date = new Date(dateString);
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    if (month > 12) {
      year = year + 1;
      month = 1;
    }
    var result = "Tháng "+ month + " - ( " + year+" )";
    return result;
  }

  const CustomToolbar = (props) => {
    return (
      <>
        <span style={{ textAlign: "center", fontSize: 18, color: "red" }}>
          {/* {props.label} */}
          {getMonthLabel(props.label)}
        </span>

        <div className="rbc-toolbar" style={{ float: "left", width: 300 }}>
          <span className="rbc-btn-group">
            <button
              style={{ width: 80 }}
              onClick={() => props.onNavigate("PREV") & prev(props.label)}
            >
              {" "}
              <Iconify icon={"grommet-icons:chapter-previous"} />
            </button>
            <button
              style={{ width: 80 }}
              onClick={() => props.onNavigate("TODAY") & toDay(props.label)}
            >
              <Iconify icon={"fluent:calendar-today-16-regular"} />
            </button>
            <button
              style={{ width: 80 }}
              onClick={() => props.onNavigate("NEXT") & next(props.label)}
            >
              <Iconify icon={"grommet-icons:chapter-next"} />
            </button>
          </span>
        </div>
      </>
    );
  };

  function Event(event) {
    return (
      <div style={{ textAlign: "center", height: 66 }}>
        <div
          style={{
            background: "#9af5c4",
            borderRadius: 10,
            padding: 5,
            color: "red",
            marginTop: 45,
          }}
        >
          {event.event.title}
        </div>
      </div>
    );
  }

  const eventSelected = (data) => {
    setTimekeepingUpdate(data);
    setIsEditUpdate(true);
  };

  return (
    <>
      {" "}
      <Helmet>
        <title> Trang chủ | Chấm công </title>
      </Helmet>
      <div>
        <p>Điểm danh</p>
        <div style={{ height: "500pt" }}>
          <Calendar
            id="calendar"
            selectable
            onSelectSlot={(slotInfo) => {
              handleSelected(slotInfo);
            }}
            components={{ event: Event, toolbar: CustomToolbar }}
            events={listEvent}
            localizer={localizer}
            views={{ month: true }}
            eventPropGetter={(event, start, end, isSelected) => {
              let newStyle = {
                backgroundColor: "white",
                color: "red",
                borderRadius: "0px 52px 0px 0px",
                marginTop: -19,
              };
              return {
                className: "",
                style: newStyle,
              };
            }}
            onSelectEvent={eventSelected}
          />
        </div>
      </div>
      <TimekeepingDialog
        open={isEdit}
        setOpen={setIsEdit}
        timekeeping={timekeeping}
        onChange={getAllTimekeeping}
        month = {month}
      />
      <TimekeepingUpdateDialog
        open={isEditUpdate}
        setOpen={setIsEditUpdate}
        timekeeping={timekeepingUpdate}
        onChange={getAllTimekeeping}
        month = {month}
      />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={showLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
  // }
};

export default Timekeeping;
